# Preloved Shop Mobile App

Allow users to donate their preloved/unwanted items in the platform

Application is built with Flutter

Tools that I used to build this application:
1. Visual Studio Code
2. Android Emulator
3. Flutter framework
4. Cloud Firestore and Firebase Storage for back-end services
5. Algolia to implement the real-time search for products (https://www.algolia.com/)
6. GitLab for version control

Download link is attached below (android only):
https://drive.google.com/open?id=18hyqZIvp_Dp4s8NrhrH_zAmzbx68bwiC

## Project Progress

Finished:
1. Login & Signup Page
2. Google login
3. Home page
4. Seller profile page
5. Add product
6. Product detail (hardcode)
7. Product Listing Page
8. Product detail (read from user’s product)
9. Chat with seller by using Whatsapp and Phone number
10. Making Review
11. Delete product
12. Add item to wishlist
13. Fixed: Using Google gmail to Login/Sign up
14. Search Product
15. Edit user product
16. Edit user profile
17. Check internet connection
---------------------------------------------------------------------------------------------------------------------------
Ongoing:
1. Realtime Chat with seller (pending)
2. Change user password
---------------------------------------------------------------------------------------------------------------------------
Issue found:
1. Unsorted reviews by timestamp
2. Unable to change user password
3. Category should not included 'Donation', instead should add one more field to store donation status which      is "Non-donation" and "Donation"
4. Update on realtime search
5. Image file size too large
---------------------------------------------------------------------------------------------------------------------------
Future improvement:
1. Set limitation on the image file size
2. Receive notification after buyer make review to seller
3. Verify phone number when user register account
4. Implement search filter
5. Implement Realtime chat
6. Implement payment gateway
7. Allow user to make report regarding selling illegal product at the platform
8. Allow seller to make report regarding to inappropriate review such as contain offensive words
---------------------------------------------------------------------------------------------------------------------------

## Documentation Progress
1. Working on UAT testing and functional testing
2. Finalize report


For help getting started with Flutter, view our 
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.
