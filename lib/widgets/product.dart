import 'package:flutter/material.dart';
import 'package:prelovedshop/pages/productDetail.dart';
import '../pages/productDetail.dart';
import 'package:firebase_auth/firebase_auth.dart';


class Product extends StatefulWidget {
  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  var product_list = [
    {
      "name": "Polo",
      "picture": "assets/products/clothes1.jpeg",
      "price": 99.0,
    },
    {
      "name": "Adidas Ultra Boost",
      "picture": "assets/products/shoes2.jpg",
      "price": 0.0,
    },
    {
      "name": "Adidas Backpack",
      "picture": "assets/products/bag1.jpg",
      "price": 130.0,
    },
    {
      "name": "Adidas Backpack 2",
      "picture": "assets/products/bag2.jpg",
      "price": 0.0,
    },
    {
      "name": "T-shirt",
      "picture": "assets/products/clothes2.jpg",
      "price": 300.0,
    }
  ];

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: product_list.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemBuilder: (BuildContext context, int index){
        return SingleProduct(
          productName: product_list[index]['name'],
          productPicture: product_list[index]['picture'],
          productPrice: product_list[index]['price'],
        );
      },
    );
  }
}

class SingleProduct extends StatelessWidget {
  final productName;
  final productPicture;
  final double productPrice;
  final FirebaseUser user;

  const SingleProduct({Key key, this.productName, this.productPicture, this.productPrice, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Hero(
        tag: Text("Hero"),
        child: Material(
          child: InkWell(
            onTap: ()=>{
              Navigator.of(context).push(MaterialPageRoute(
                //Passing values to the product detail page
                builder: (context) => ProductDetail(
                  productName: this.productName,
                  productPrice: this.productPrice,
                  productImage: this.productPicture,
                  user: user,
                )
              ))
            },
            child: GridTile(
              footer: Container(
                color: Colors.white70,
                child: 
                // Row(
                //   children: <Widget>[
                //     Expanded(
                //       child: Text(
                //         productName,
                //         style: TextStyle(
                //           fontWeight: FontWeight.bold,
                //           fontSize: 14.0,
                //         ),
                //       ),
                //     ),
                //     //ProductPrice Condition
                //     productPrice != 0 ?
                //     Text(
                //       "RM "+productPrice.toString(),
                //       style: TextStyle(
                //         color: Colors.pink[400],
                //         fontWeight: FontWeight.w500,
                //         fontSize: 15.0
                //       ),
                //     ) : Text(
                //       "Donation",
                //       style: TextStyle(
                //         color: Colors.pink[400],
                //         fontWeight: FontWeight.w500,
                //         fontSize: 14.0
                //       ),
                //     ),
                //   ],
                // )
                
                ListTile(
                  //Price condition
                  title: productPrice != 0 ?
                  Text("RM "+productPrice.toString(), style: TextStyle(
                    color: Colors.pink[400],
                    fontWeight: FontWeight.w500,
                    fontSize: 14.0,
                    ),
                  ) 
                  : Text("Donation", style: TextStyle(
                      color: Colors.pink[400],
                      fontWeight: FontWeight.w500,
                      fontSize: 12.5,
                    ),
                  ),
                  // ===========================
                  leading: Container(
                    width: 75.0,
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 4.0),
                      child: Text(productName, style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0,
                        )
                      )
                    )
                  ),
                ),
              ),
              child: Image.asset(productPicture, fit: BoxFit.cover,),
            ),
          ),
        ),
      ),
    );
  }
}