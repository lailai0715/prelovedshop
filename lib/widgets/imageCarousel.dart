import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';

class ImageCarousel extends StatefulWidget {
  @override
  _ImageCarouselState createState() => _ImageCarouselState();
}

class _ImageCarouselState extends State<ImageCarousel> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150.0,
      child: Carousel(
        boxFit: BoxFit.cover,
        images: [
          AssetImage('assets/c1.jpg'),
          AssetImage('assets/c2.jpg'),
          AssetImage('assets/c3.jpg')
        ],
        dotSize: 2.0,
        animationDuration: Duration(seconds: 1),
        animationCurve: Curves.fastOutSlowIn,
        indicatorBgPadding: 6.0,
        dotColor: Colors.white,
        dotBgColor: Colors.transparent,
      ),
    );
  }
}