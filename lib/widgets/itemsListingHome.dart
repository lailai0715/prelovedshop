import 'package:flutter/material.dart';

import 'product.dart';

class ItemListingHomePage extends StatefulWidget {
  @override
  _ItemListingHomePageState createState() => _ItemListingHomePageState();
}

class _ItemListingHomePageState extends State<ItemListingHomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 320.0,
      child: Product(),
    );
  }
}