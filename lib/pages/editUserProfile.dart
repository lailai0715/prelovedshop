import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import './homePage.dart';

class EditUserProfilePage extends StatefulWidget {
  final FirebaseUser user;
  final String userName;
  final String phoneNumber;

  const EditUserProfilePage(
      {Key key, this.user, this.userName, this.phoneNumber})
      : super(key: key);

  @override
  _EditUserProfilePageState createState() => _EditUserProfilePageState();
}

class _EditUserProfilePageState extends State<EditUserProfilePage> {
  bool isLoading = false;
  String _password;
  String _username;
  String _phoneNo;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _passwordFormKey = GlobalKey<FormState>();
  List<String> _productlistIds = [];
  List<String> _reviewsIds = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //print(widget.user.providerData[1].providerId);
  }

  void showUpdatePasswordAlert() async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Form(
              key: _passwordFormKey,
              child: TextFormField(
                //controller: _passwordTextController,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Invalid password';
                  } else if (value.length < 6) {
                    return "Password has to be atleast 6 characters long";
                  }
                },
                onSaved: (input) => _password = input,
                style: TextStyle(color: Colors.black),
                //initialValue: '********',
                obscureText: true,
                decoration: InputDecoration(
                  hintText: 'Enter new password',
                  icon: Icon(
                    Icons.lock_outline,
                    color: Colors.black,
                  ),
                  // contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                  // border: OutlineInputBorder(
                  //   borderRadius: BorderRadius.circular(32.0),
                  // )
                ),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('CANCEL'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text('UPDATE', style: TextStyle(color: Colors.red)),
                onPressed: () async {
                  final formState = _passwordFormKey.currentState;
                  if (formState.validate()) {
                    setState(() {
                      isLoading = true;
                    });
                    formState.save();

                    // updatePassword(_password);

                    
                    // widget.user.getIdToken().then((onValue){
                    //   updateUserProfile(
                    //   );
                    // });

                    // widget.user.updatePassword(_password).then((onValue) {
                    //   Fluttertoast.showToast(msg: "Password has been changed");
                    // }).catchError((onError) {
                    //   //print("Password can't be changed" + onError.toString());
                    //   Fluttertoast.showToast(msg: "Password can't be changed " + onError.toString());
                    // });

                    Navigator.of(context).pop();
                    setState(() {
                      isLoading = false;
                    });
                  } else {
                    setState(() {
                      isLoading = false;
                    });
                    Fluttertoast.showToast(msg: "Form validate failed");
                  }
                },
              )
            ],
          );
        });
  }

  void updateUserProfile() async {
    final formState = _formKey.currentState;
    if (formState.validate()) {
      setState(() {
        isLoading = true;
      });
      formState.save();

      QuerySnapshot productDetails = await Firestore.instance.collection('products')
                                            .where('userId', isEqualTo: widget.user.uid)
                                            .getDocuments();

      QuerySnapshot reviewDetails = await Firestore.instance.collection('reviews')
                                          .where('reviewerUserId', isEqualTo: widget.user.uid)
                                          .getDocuments();
      
      if(productDetails.documents.length > 0 ){
        for(int i = 0 ; i<productDetails.documents.length; i++){
          _productlistIds.insert(i, productDetails.documents[i].data['id']);
        }
      }

      for (int i = 0; i < _productlistIds.length; i++) {
      await Firestore.instance
          .collection('products')
          .document(_productlistIds[i])
          .updateData({
        'username': _username,
      }).catchError((onError) => print(onError));
    }

    if(reviewDetails.documents.length > 0){
      for(int i = 0; i<reviewDetails.documents.length; i++){
        _reviewsIds.insert(i, reviewDetails.documents[i].data['id']);
      }
    }

    for(int i = 0; i<_reviewsIds.length; i++){
      await Firestore.instance
          .collection('reviews')
          .document(_reviewsIds[i])
          .updateData({
            'reviewerUsername': _username,
          }).catchError((onError) => print(onError));
    }
      

      await Firestore.instance
          .collection('users')
          .document(widget.user.uid)
          .updateData({
        'username': _username,
        'phone_number': _phoneNo,
      }).catchError((onError) => print(onError));
      //print(_password);

      // widget.user.updatePassword(_password).then((onValue) {
      //   print("Successful changed password");
      // }).catchError((onError) {
      //   print("Password can't be changed" + onError.toString());
      // });

      _formKey.currentState.reset();

      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(msg: "Profile is updated");
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => HomePage(
                    user: widget.user,
                  )));
    } else {
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(msg: "Form validate failed");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit user profile'),
        backgroundColor: Colors.pink[400],
      ),
      body: Stack(children: <Widget>[
        Container(
          color: Colors.white,
          child: Center(
            child: Form(
              key: _formKey,
              child: ListView(
                //controller: _listViewScrollController,
                shrinkWrap: true,
                padding: EdgeInsets.only(left: 24.0, right: 24.0),
                children: <Widget>[
                  SizedBox(height: 20.0),
                  widget.user.providerData[1].providerId == "password"
                      ? SizedBox()
                      : Text(
                          "Logged in with Google Gmail, password can't be changed",
                          style: TextStyle(color: Colors.grey),
                        ),

                  SizedBox(height: 18.0),

                  TextFormField(
                    //controller: _usernameTextController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Can\'t be empty';
                      } else if (value.length < 6) {
                        return "Must be at least 6 characters long";
                      }
                    },
                    onSaved: (input) => _username = input,
                    style: TextStyle(color: Colors.black),
                    initialValue: widget.userName,
                    decoration: InputDecoration(
                      hintText: 'Username',
                      icon: Icon(
                        Icons.person_outline,
                        color: Colors.black,
                      ),
                      // contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      // border: OutlineInputBorder(
                      //   borderRadius: BorderRadius.circular(32.0),
                      // )
                    ),
                  ),
                  SizedBox(height: 8.0),
                  // TextFormField(
                  //   controller: _emailTextController,
                  //   validator: (value) {
                  //     if (value.isEmpty ||
                  //         !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                  //             .hasMatch(value)) {
                  //       return 'A valid Email is required';
                  //     }
                  //   },
                  //   onSaved: (input) => _email = input,
                  //   style: TextStyle(color: Colors.black),
                  //   keyboardType: TextInputType.emailAddress,
                  //   //initialValue: 'prelovedshop@gmail.com',
                  //   decoration: InputDecoration(
                  //     hintText: 'Email',
                  //     icon: Icon(
                  //       Icons.mail_outline,
                  //       color: Colors.black,
                  //     ),
                  //     // contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                  //     // border: OutlineInputBorder(
                  //     // borderRadius: BorderRadius.circular(32.0),
                  //     // )
                  //   ),
                  // ),
                  SizedBox(height: 8.0),

                  SizedBox(height: 8.0),
                  TextFormField(
                    //controller: _phoneNoTextController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Can\'t be empty';
                      } else if (!RegExp(r"[0-9]").hasMatch(value)) {
                        return "Numbers only";
                      } else if (value.length < 10) {
                        return "Must be at least 10 numbers";
                      }
                    },
                    onSaved: (input) => _phoneNo = input,
                    keyboardType: TextInputType.phone,
                    style: TextStyle(color: Colors.black),
                    initialValue: widget.phoneNumber,
                    decoration: InputDecoration(
                      hintText: 'Phone Number',
                      icon: Icon(
                        Icons.phone,
                        color: Colors.black,
                      ),
                      // contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      // border: OutlineInputBorder(
                      //   borderRadius: BorderRadius.circular(32.0),
                      // )
                    ),
                  ),
                  SizedBox(height: 30.0),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 16.0),
                    child: Material(
                      borderRadius: BorderRadius.circular(20.0),
                      shadowColor: Colors.lightBlueAccent.shade100,
                      child: MaterialButton(
                        minWidth: 200.0,
                        height: 42.0,
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            print('Validate Success');
                            updateUserProfile();
                          }
                        },
                        color: Colors.pink[400],
                        child:
                            Text('Save', style: TextStyle(color: Colors.white)),
                      ),
                    ),
                  ),
                  widget.user.providerData[1].providerId == "password"
                      ? Padding(
                          padding: EdgeInsets.symmetric(vertical: 10.0),
                          child: Material(
                            borderRadius: BorderRadius.circular(20.0),
                            shadowColor: Colors.lightBlueAccent.shade100,
                            child: MaterialButton(
                              minWidth: 200.0,
                              height: 42.0,
                              onPressed: () {
                                showUpdatePasswordAlert();
                              },
                              color: Colors.blue[400],
                              child: Text('Change Password (Not working)',
                                  style: TextStyle(color: Colors.white)),
                            ),
                          ),
                        )
                      : SizedBox()
                ],
              ),
            ),
          ),
        ),
        Positioned(
          child: Visibility(
            visible: isLoading ?? true,
            child: Center(
              child: SimpleDialog(
                  elevation: 0.0,
                  backgroundColor: Colors.transparent,
                  children: <Widget>[
                    Center(
                      //color: Colors.white.withOpacity(0.9),
                      child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.pink[400]),
                      ),
                    ),
                  ]),
            ),
          ),
        )
      ]),
    );
  }
}
