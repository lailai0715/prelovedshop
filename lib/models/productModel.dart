class ProductModel {
  String name;
  String timestamp;
  String title;
  String description;
  String categoryId;
  String userId;
  String price;
  String condition;
  String location;
  String imageUrl;

  ProductModel() {
    this.name = "";
    this.timestamp = "";
    this.title = "";
    this.description = "";
    this.categoryId = "";
    this.userId = "";
    this.price = "";
    this.condition = "";
    this.location = "";
    this.imageUrl = "";
  }

  ProductModel.fromJson(Map<String, dynamic> data){
    this.name = data['name'];
    this.timestamp = data['timestamp'];
    this.title = data['title'];
    this.description = data['description'];
    this.categoryId = data['categoryId'];
    this.userId = data['userId'];
    this.price = data['price'];
    this.condition = data['condition'];
    this.location = data['location'];
    this.imageUrl = data['imageUrl'];
  }
}
