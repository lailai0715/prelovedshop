import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:prelovedshop/pages/loginPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:prelovedshop/pages/homePage.dart';
//import '../db/users.dart';
import './googleSignUpPage.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  String _email;
  String _password;
  String _username;
  String _phoneNo;
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  SharedPreferences sharedPreferences;
  bool isLoading = false;
  bool isLogedIn = false;
  //bool _success;
  //String _userID;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  //UserServices _userServices;
  TextEditingController _usernameTextController = TextEditingController();
  TextEditingController _emailTextController = TextEditingController();
  TextEditingController _passwordTextController = TextEditingController();
  TextEditingController _phoneNoTextController = TextEditingController();

  @override
  void initState() {
    super.initState();
    isSignedIn();
  }

  void isSignedIn() async {
    setState(() {
      isLoading = true;
    });

    FirebaseUser existingUser = await FirebaseAuth.instance.currentUser();

    await firebaseAuth.currentUser().then((user) {
      if (user != null) {
        setState(() => isLogedIn = true);
      }
    });
    if (isLogedIn) {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => HomePage(user: existingUser)));
    }

    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create a new account'),
        backgroundColor: Colors.pink[400],
      ),
      body: Stack(children: <Widget>[
        Container(
          color: Colors.white,
          child: Center(
            child: Form(
              key: _formKey,
              child:
                  ListView(
                //controller: _listViewScrollController,
                shrinkWrap: true,
                padding: EdgeInsets.only(left: 24.0, right: 24.0),
                children: <Widget>[
                  Hero(
                    tag: 'hero',
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: 48.0,
                      child: Image.asset('assets/logo.PNG'),
                    ),
                  ),
                  SizedBox(height: 20.0),

                  TextFormField(
                    controller: _usernameTextController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Can\'t be empty';
                      } else if(value.length < 6) {
                        return "Must be at least 6 characters long";
                      }
                    },
                    onSaved: (input) => _username = input,
                    style: TextStyle(color: Colors.black),
                    //initialValue: '********',
                    decoration: InputDecoration(
                      hintText: 'Username',
                      icon: Icon(
                        Icons.person_outline,
                        color: Colors.black,
                      ),
                      // contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      // border: OutlineInputBorder(
                      //   borderRadius: BorderRadius.circular(32.0),
                      // )
                    ),
                  ),
                  SizedBox(height: 8.0),
                  TextFormField(
                    controller: _emailTextController,
                    validator: (value) {
                      if (value.isEmpty ||
                          !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                              .hasMatch(value)) {
                        return 'A valid Email is required';
                      }
                    },
                    onSaved: (input) => _email = input,
                    style: TextStyle(color: Colors.black),
                    keyboardType: TextInputType.emailAddress,
                    //initialValue: 'prelovedshop@gmail.com',
                    decoration: InputDecoration(
                      hintText: 'Email',
                      icon: Icon(
                        Icons.mail_outline,
                        color: Colors.black,
                      ),
                      // contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      // border: OutlineInputBorder(
                      // borderRadius: BorderRadius.circular(32.0),
                      // )
                    ),
                  ),
                  SizedBox(height: 8.0),
                  TextFormField(
                    controller: _passwordTextController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Invalid password';
                      } else if (value.length < 6) {
                        return "Password has to be atleast 6 characters long";
                      }
                    },
                    onSaved: (input) => _password = input,
                    style: TextStyle(color: Colors.black),
                    //initialValue: '********',
                    obscureText: true,
                    decoration: InputDecoration(
                      hintText: 'Password',
                      icon: Icon(
                        Icons.lock_outline,
                        color: Colors.black,
                      ),
                      // contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      // border: OutlineInputBorder(
                      //   borderRadius: BorderRadius.circular(32.0),
                      // )
                    ),
                  ),
                  SizedBox(height: 8.0),
                  TextFormField(
                    controller: _phoneNoTextController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Can\'t be empty';
                      } else if (!RegExp(r"[0-9]").hasMatch(value)) {
                        return "Numbers only";
                      } else if(value.length < 10) {
                        return "Must be at least 10 numbers";
                      }
                    },
                    onSaved: (input) => _phoneNo = input,
                    keyboardType: TextInputType.phone,
                    style: TextStyle(color: Colors.black),
                    //initialValue: '********',
                    decoration: InputDecoration(
                      hintText: 'Phone Number',
                      icon: Icon(
                        Icons.phone,
                        color: Colors.black,
                      ),
                      // contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      // border: OutlineInputBorder(
                      //   borderRadius: BorderRadius.circular(32.0),
                      // )
                    ),
                  ),
                  SizedBox(height: 30.0),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 16.0),
                    child: Material(
                      borderRadius: BorderRadius.circular(20.0),
                      shadowColor: Colors.lightBlueAccent.shade100,
                      child: MaterialButton(
                        minWidth: 200.0,
                        height: 42.0,
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            print('Validate Success');
                            signUp();
                          }
                        },
                        color: Colors.pink[400],
                        child: Text('Sign Up',
                            style: TextStyle(color: Colors.white)),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Material(
                        borderRadius: BorderRadius.circular(50.0),
                        shadowColor: Colors.lightBlueAccent.shade100,
                        child: Container(
                            width: 200.0,
                            height: 42.0,
                            child: FlatButton.icon(
                              onPressed: () {
                                handleSignIn();
                              },
                              label: Text(
                                "  Sign up with Google",
                                style: TextStyle(color: Colors.white),
                              ),
                              color: Colors.red.shade900,
                              icon: Icon(
                                FontAwesomeIcons.google,
                                color: Colors.white,
                              ),
                            ))),
                  ),
                  FlatButton(
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) => LoginPage()));
                    },
                    child: Text('Already have account? Log in now',
                        style: TextStyle(
                            color: Colors.blue,
                            decoration: TextDecoration.underline)),
                  ),
                  //SizedBox(height: 10.0)
                ],
              ),
            ),
          ),
        ),
        Positioned(
          child: Visibility(
            visible: isLoading ?? true,
            child: Center(
              child: SimpleDialog(
                  elevation: 0.0,
                  backgroundColor: Colors.transparent,
                  children: <Widget>[
                    Center(
                      //color: Colors.white.withOpacity(0.9),
                      child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.pink[400]),
                      ),
                    ),
                  ]),
            ),
          ),
        )
      ]),
    );
  }

  Future<void> signUp() async {
    final formState = _formKey.currentState;
    setState(() {
      isLoading = true;
    });
    if (formState.validate()) {
      //Login to firebase
      formState.save();
      try {
        FirebaseUser user = await FirebaseAuth.instance
            .createUserWithEmailAndPassword(email: _email, password: _password);
        user.sendEmailVerification();
        Firestore.instance.collection("users").document(user.uid).setData({
          "id": user.uid,
          "username": _username,
          "email": _email,
          "profilePicture": "",
          "timestamp": Timestamp.now(),
          "phone_number": _phoneNo,
        });

        //Display for the user that we sent an email
        //Navigate to home
        //Navigator.of(context).pop();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => HomePage(
                      user: user,
                    )));
        Fluttertoast.showToast(
          msg: "Sign up successful",
        );
        setState(() {
          isLoading = false;
        });
      } catch (e) {
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(
          msg: "Account already exist",
        );
        print(e.message);
      }
    }
    //  if (formState.validate()) {
    //   FirebaseUser user = await firebaseAuth.currentUser();
    //   if (user == null) {
    //     firebaseAuth
    //         .createUserWithEmailAndPassword(
    //             email: _emailTextController.text,
    //             password: _passwordTextController.text)
    //         .then((user) => {
    //           _userServices.createUser(
    //         {
    //         "username": _usernameTextController.text,
    //         "email": _emailTextController.text,
    //         "userId": user.uid,
    //         }
    //     )
    //     }).catchError((err) => {print('error is: '+ err.toString())});
  }

  Future<void> handleSignIn() async {
    setState(() {
      isLoading = true;
    });

    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;
    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    final FirebaseUser user =
        await firebaseAuth.signInWithCredential(credential);
    assert(user.email != null);
    assert(user.displayName != null);
    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await firebaseAuth.currentUser();
    assert(user.uid == currentUser.uid);

    //sharedPreferences = await SharedPreferences.getInstance();

    // setState(() {
    //   isLoading = true;
    // });

    // GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    // GoogleSignInAuthentication googleSignInAuthentication =
    //     await googleSignInAccount.authentication;
    // //FirebaseUser firebaseUser = await firebaseAuth.signInWithGoogle(idToken: null, accessToken: null)
    // AuthCredential credential = GoogleAuthProvider.getCredential(
    //     accessToken: googleSignInAuthentication.accessToken,
    //     idToken: googleSignInAuthentication.idToken);

    // FirebaseUser firebaseUser =
    //     await firebaseAuth.signInWithCredential(credential);

    if (user != null) {
      final QuerySnapshot result = await Firestore.instance
          .collection("users")
          .where("id", isEqualTo: user.uid)
          .getDocuments();
      final List<DocumentSnapshot> documents = result.documents;

      //If list is empty, means no have any user exist in the collection, it will insert into firestore
      if (documents.length == 0) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => GoogleSignUp(
                      user: user,
                    )));
        // await sharedPreferences.setString("id", user.uid);
        // await sharedPreferences.setString("username", user.displayName);
        // await sharedPreferences.setString("email", user.email);
        // await sharedPreferences.setString("photoUrl", user.photoUrl);
      } else {
        // await sharedPreferences.setString("id", documents[0]['id']);
        // await sharedPreferences.setString("username", documents[0]['username']);
        // await sharedPreferences.setString("email", documents[0]['email']);
        // await sharedPreferences.setString("photoUrl", documents[0]['photoUrl']);
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => HomePage(
                      user: user,
                    )));

        Fluttertoast.showToast(msg: "Log in successful");
      }
      setState(() {
        if (user != null) {
          //_success = true;
          //_userID = user.uid;
          isLoading = false;
        } else {
          //_success = false;
          isLoading = false;
        }
      });
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }
}
