import 'package:flutter/material.dart';
//import 'package:prelovedshop/pages/addProduct.dart';
//import 'package:prelovedshop/widgets/product.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../db/users.dart';
//import '../db/product.dart';
//import 'dart:io';
import 'productDetail.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../db/review.dart';
import 'package:timeago/timeago.dart' as timeago;

class SellerProfile extends StatefulWidget {
  final FirebaseUser user;
  final String userId;
  const SellerProfile({Key key, this.userId, this.user}) : super(key: key);

  @override
  _SellerProfileState createState() => _SellerProfileState();
}

class _SellerProfileState extends State<SellerProfile> {
  String _comment;
  String _reviewerUserName;
  String _sellerUserId;
  UserServices _userServices = UserServices();
  TextEditingController _commentTextController = TextEditingController();
  List<String> reviews = ['review1', 'review2', 'review3'];
  bool isLoading = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  ReviewServices _reviewSerivices = ReviewServices();
  //Firestore _firestore = Firestore.instance;
  bool isReviewed = false;

  @override
  void initState() {
    super.initState();
    // TODO: implement initState
    _getUserDetail();
    _sellerUserId = widget.user.uid.toString();
    print('Seller user is: ' + widget.userId);
    print('Login user is: ' + widget.user.uid);
  }

  void _getUserDetail() async {
    List<DocumentSnapshot> userdata = await _userServices.getUser(widget.user);
    setState(() {
      _reviewerUserName = userdata[0].data['username'].toString();
      print("Reviewer username is: " + _reviewerUserName);
    });
  }

  _getDate(Timestamp t) {
    return timeago.format(t.toDate(), locale: 'en_short');
  }

  void _showMakeReviewDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return Stack(children: <Widget>[
            Dialog(
                child: Container(
              width: 350.0,
              height: 350.0,
              child: Column(children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
                      child: IconButton(
                        icon: Icon(Icons.close),
                        color: Colors.grey,
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(bottom: 10.0),
                      child: Text(
                        'Make Review',
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 300.0,
                      //height: 150.0,
                      child: Padding(
                        padding: EdgeInsets.only(bottom: 20.0, top: 20.0),
                        child: Form(
                          key: _formKey,
                          child: TextFormField(
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'Write your review here',
                                //helperText: 'Keep it short, this is just a demo.',
                                labelText: 'Write your review here',
                                labelStyle: TextStyle()),
                            maxLines: 5,
                            controller: _commentTextController,
                            onSaved: (input) => _comment = input,
                            validator: (String value) {
                              if (value.isEmpty) {
                                return 'Can not be empty';
                              }
                            },
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          addReview();
                          Navigator.of(context).pop();
                        }
                      },
                      child: Text('Submit'),
                      color: Colors.pink[400],
                      textColor: Colors.white,
                    )
                  ],
                )
              ]),
            )),
            Positioned(
              child: Visibility(
                visible: isLoading ?? true,
                child: Center(
                  child: SimpleDialog(
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      children: <Widget>[
                        Center(
                          //color: Colors.white.withOpacity(0.9),
                          child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.pink[400]),
                          ),
                        ),
                      ]),
                ),
              ),
            )
          ]);
        });
  }

  void addReview() async {
    final formState = _formKey.currentState;
    if (formState.validate()) {
      formState.save();
      setState(() {
        isLoading = true;
      });
      if (_comment != null) {
        //final FirebaseStorage storage = FirebaseStorage.instance;

        //StorageTaskSnapshot snapshot3 = await task3.onComplete.then((snapshot) => snapshot);
        _reviewSerivices.addReview(
          _comment,
          widget.user,
          widget.userId,
          _reviewerUserName,
        );

        _formKey.currentState.reset();

        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(msg: "Make Review successfully");
      } else {
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(msg: "Review can not be blank");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        floatingActionButton: Container(
          height: 40.0,
          width: 120.0,
          child: FloatingActionButton.extended(
            elevation: 5.0,
            icon: Icon(Icons.chat_bubble_outline),
            label: Text("Review"),
            onPressed: () {
              _showMakeReviewDialog();
            },
            backgroundColor: Colors.pink[400],
            shape: RoundedRectangleBorder(),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.pink[400],
          title: Text('Seller Profile'),
        ),
        body: StreamBuilder<QuerySnapshot>(
          stream: Firestore.instance
              .collection('products')
              .where('userId', isEqualTo: widget.userId)
              .snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            }
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return Center(
                  child: SimpleDialog(
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      children: <Widget>[
                        Center(
                          //color: Colors.white.withOpacity(0.9),
                          child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.pink[400]),
                          ),
                        ),
                      ]),
                );
              default:
                return Column(
                  children: <Widget>[
                    Container(
                        height: 130.0,
                        color: Colors.pink[400],
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 0.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                CircleAvatar(
                                  radius: 45.0,
                                  backgroundImage:
                                      AssetImage('assets/avatar.png'),
                                ),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 10.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    snapshot.data.documents[0]['username'],
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20.0,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        )),
                    Container(
                      height: 40.0,
                      color: Colors.pink[400],
                      child: TabBar(
                        //controller: tabController,
                        labelColor: Colors.white,
                        tabs: <Widget>[
                          Tab(
                            text: "LISTING",
                          ),
                          Tab(
                            text: "REVIEW",
                          )
                        ],
                      ),
                    ),
                    Flexible(
                      child: TabBarView(
                        children: <Widget>[
                          //Render Listing Tab
                          snapshot.data.documents.length != 0
                              ? Container(
                                  child: GridView.builder(
                                    itemCount: snapshot.data.documents.length,
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount: 2),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Card(
                                        child: Hero(
                                          tag: Text("Hero"),
                                          child: Material(
                                            child: InkWell(
                                              onTap: () => {
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                        //Passing values to the product detail page
                                                        builder: (context) =>
                                                            ProductDetail(
                                                              productId: snapshot
                                                                      .data
                                                                      .documents[
                                                                  index]['id'],
                                                              user: widget.user,
                                                              username: snapshot
                                                                          .data
                                                                          .documents[
                                                                      index]
                                                                  ['username'],
                                                            )))
                                              },
                                              child: GridTile(
                                                footer: Container(
                                                  color: Colors.white70,
                                                  child: ListTile(
                                                    //Price condition
                                                    title:
                                                        snapshot.data.documents[
                                                                        index]
                                                                    ['price'] !=
                                                                0
                                                            ? Text(
                                                                "RM" +
                                                                    snapshot
                                                                        .data
                                                                        .documents[index]
                                                                            [
                                                                            'price']
                                                                        .toString() +
                                                                    "0",
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                          .pink[
                                                                      400],
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                  fontSize:
                                                                      14.0,
                                                                ),
                                                              )
                                                            : Text(
                                                                "Donation",
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                          .pink[
                                                                      400],
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                  fontSize:
                                                                      12.5,
                                                                ),
                                                              ),
                                                    // ===========================
                                                    leading: Container(
                                                        width: 75.0,
                                                        child: Padding(
                                                            padding: EdgeInsets
                                                                .symmetric(
                                                                    vertical:
                                                                        4.0),
                                                            child: Text(
                                                                snapshot.data
                                                                            .documents[
                                                                        index]
                                                                    ['title'],
                                                                style:
                                                                    TextStyle(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize:
                                                                      14.0,
                                                                )))),
                                                  ),
                                                ),
                                                child: Image.network(
                                                  snapshot.data.documents[index]
                                                      ['imageUrl'][0],
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                )
                              : Container(
                                  child: Center(
                                    child: Text("No items found"),
                                  ),
                                ),
                          //Render review tab
                          StreamBuilder<QuerySnapshot>(
                              stream: Firestore.instance
                                  .collection("reviews")
                                  .where("sellerUserId",
                                      isEqualTo: widget.userId)
                                  .snapshots(),
                              builder: (
                                BuildContext context,
                                AsyncSnapshot<QuerySnapshot> snapshot,
                              ) {
                                if (snapshot.hasError) {
                                  return Text('Error: ${snapshot.error}');
                                }
                                switch (snapshot.connectionState) {
                                  case ConnectionState.waiting:
                                    return Center(
                                      child: SimpleDialog(
                                          elevation: 0.0,
                                          backgroundColor: Colors.transparent,
                                          children: <Widget>[
                                            Center(
                                              //color: Colors.white.withOpacity(0.9),
                                              child: CircularProgressIndicator(
                                                valueColor:
                                                    AlwaysStoppedAnimation<
                                                            Color>(
                                                        Colors.pink[400]),
                                              ),
                                            ),
                                          ]),
                                    );
                                  default:
                                    return snapshot.data.documents.length != 0
                                        ? Container(
                                            child: ListView.builder(
                                              itemCount: snapshot
                                                  .data.documents.length,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 5.0),
                                                  child: ListTile(
                                                      leading: Image.asset(
                                                          'assets/avatar.png'),
                                                      title: Text(
                                                        snapshot.data.documents[
                                                                index][
                                                            'reviewerUsername'],
                                                        style: TextStyle(
                                                          color: Colors.blue,
                                                        ),
                                                      ),
                                                      subtitle: Text(
                                                        snapshot.data.documents[
                                                            index]['comment'],
                                                        style: TextStyle(
                                                            color:
                                                                Colors.black),
                                                      ),
                                                      trailing: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .end,
                                                        children: <Widget>[
                                                          Icon(Icons.schedule,
                                                              color:
                                                                  Colors.grey),
                                                          Text(
                                                            _getDate(snapshot
                                                                            .data
                                                                            .documents[
                                                                        index][
                                                                    'timestamp'])
                                                                .toString(),
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 13.0,
                                                                color: Colors
                                                                    .grey),
                                                          )
                                                        ],
                                                      )),
                                                );
                                              },
                                            ),
                                          )
                                        : Container(
                                            child: Center(
                                              child: Text("No reviews found"),
                                            ),
                                          );
                                }
                              })
                        ],
                      ),
                    )
                  ],
                );
            }
          },
        ),
      ),
    );
  }
}
