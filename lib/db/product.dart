import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:prelovedshop/models/productModel.dart';
import 'package:uuid/uuid.dart';

class ProductServices{
  Firestore firestore = Firestore.instance;
  List<ProductModel> products = [];

  void addProduct(String title, String category, String description, double price, 
  String location, String condition, FirebaseUser user, List imageUrlList, String username)  {
    var pid = Uuid();
    String productId = pid.v1();

    //  firestore.collection('categories').document(categoryId).setData({
    //   "name": category
    // }).catchError((error)=>print(error));

     firestore.collection('products').document(productId).setData({
      "id": productId,
      "title": title,
      "description": description,
      "price": price,
      "category": category,
      "location": location,
      "condition": condition,
      "userId": user.uid,
      "username": username,
      "timestamp": Timestamp.now(),
      "imageUrl" : imageUrlList,
    }).catchError((error)=>print(error));
  }

  void addProduct2(Map details){
    var id = Uuid();
    String productId = id.v1();

    firestore.collection('products').document(productId).setData(details)
    .catchError((error)=>print(error));
  }

  Future<List<DocumentSnapshot>> getUserProducts(FirebaseUser user) =>
      firestore.collection('products')
      .where('userId', isEqualTo: user.uid)
      .getDocuments()
      .then((snapshot) => snapshot.documents);

  getAllProducts() async {
    QuerySnapshot querySnapshot = await Firestore.instance.collection("products").getDocuments();

    querySnapshot.documents.forEach((f){
      // Using the named constructor
      products.add(ProductModel.fromJson(f.data));
    });
  }

  Future<QuerySnapshot> getProductDetails(String productId){
    return firestore.collection('products').where('id', isEqualTo: productId).getDocuments();
  }
}