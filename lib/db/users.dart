import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class UserServices {
  Firestore _firestore = Firestore.instance;
  String collection = "users";

  void createUser(Map value){
    _firestore.collection("users").document(value["id"]).setData(value).catchError((error)=>{
      print(error)
    });
  }

  Future<List<DocumentSnapshot>> getUser(FirebaseUser user) {
      return _firestore.collection('users').where('id', isEqualTo: user.uid)
      .getDocuments()
      .then((snapshot) {
        return snapshot.documents;
      });
  }
}