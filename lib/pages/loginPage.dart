import 'dart:async';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
//import 'package:prelovedshop/db/users.dart';
import 'package:prelovedshop/pages/homePage.dart';
import 'package:prelovedshop/pages/signUpPage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
//import 'dart:convert';
import './googleSignUpPage.dart';
import 'package:connectivity/connectivity.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _email;
  String _password;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  ScrollController _listViewScrollController = new ScrollController();
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  SharedPreferences sharedPreferences;
  bool isLoading = false;
  bool isLogedIn = false;
  //bool _success;
  //String _userID;
  bool hidePass = true;
  bool isConnectedToInternet;

  @override
  void initState() {
    super.initState();
    checkConnectivity();
    //isSignedIn();
  }

  void checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      setState(() {
        isConnectedToInternet = true;
      });
      isSignedIn();
    } else {
      setState(() {
        isConnectedToInternet = false;
      });
      FirebaseAuth.instance.signOut();
    }
  }

  void isSignedIn() async {
    setState(() {
      isLoading = true;
    });

    FirebaseUser existingUser = await FirebaseAuth.instance.currentUser();

    await firebaseAuth.currentUser().then((user) {
      if (user != null) {
        setState(() => isLogedIn = true);
      }
    });
    if (isLogedIn) {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => HomePage(user: existingUser)));
    }

    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Log in to your account'),
        backgroundColor: Colors.pink[400],
      ),
      body: Container(
        color: Colors.white,
        child: Stack(
          children: <Widget>[
            Container(
              child: Center(
                child: Form(
                  key: _formKey,
                  child:
                      // Column(
                      //   children: <Widget>[
                      //     TextFormField(
                      //       validator: (input) {
                      //         if (input.isEmpty) {
                      //           return 'Please type an email';
                      //         }
                      //       },
                      //       onSaved: (input) => _email = input,
                      //       decoration: InputDecoration(labelText: 'Email'),
                      //     ),
                      //     TextFormField(
                      //       validator: (input) {
                      //         if (input.length < 6) {
                      //           return 'Password needs to be atleast 6 characters';
                      //         }
                      //       },
                      //       onSaved: (input) => _password = input,
                      //       decoration: InputDecoration(labelText: 'Password'),
                      //       obscureText: true,
                      //     ),
                      //     RaisedButton(
                      //       onPressed: login,
                      //       child: Text("Log In"),
                      //     )
                      //   ],
                      // ),
                      ListView(
                    controller: _listViewScrollController,
                    shrinkWrap: true,
                    padding: EdgeInsets.only(left: 24.0, right: 24.0),
                    children: <Widget>[
                      //----------------Logo of the app----------------

                      CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 48.0,
                        child: Image.asset('assets/logo.PNG'),
                      ),

                      SizedBox(height: 48.0),

                      //----------------Email Text Field----------------
                      Material(
                        color: Colors.white,
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty ||
                                !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                                    .hasMatch(value)) {
                              return "A valid Email is required";
                            }
                          },
                          onSaved: (input) => _email = input,
                          style: TextStyle(color: Colors.black),
                          keyboardType: TextInputType.emailAddress,
                          initialValue: 'prelovedshop@gmail.com',
                          decoration: InputDecoration(
                            hintText: 'Email',
                            icon: Icon(
                              Icons.mail_outline,
                              color: Colors.black,
                            ),
                            // contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                            // border: OutlineInputBorder(
                            // borderRadius: BorderRadius.circular(32.0),
                            // )
                          ),
                        ),
                      ),

                      SizedBox(height: 8.0),

                      //----------------Password Text Field----------------
                      TextFormField(
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Can\'t be empty';
                          } else if (value.length < 6) {
                            return "Password has to be atleast 6 characters long";
                          }
                        },
                        onSaved: (input) => _password = input,
                        style: TextStyle(color: Colors.black),
                        initialValue: '********',
                        obscureText: hidePass,
                        decoration: InputDecoration(
                          hintText: 'Password',
                          icon: Icon(
                            Icons.lock_outline,
                            color: Colors.black,
                          ),
                          // contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                          // border: OutlineInputBorder(
                          //   borderRadius: BorderRadius.circular(32.0),
                          // )
                        ),
                      ),

                      //----------------Forgot Password button----------------
                      FlatButton(
                        onPressed: () {},
                        child: Text('Forgot password?',
                            style: TextStyle(color: Colors.grey)),
                      ),

                      SizedBox(height: 15.0),

                      //----------------Login Button----------------
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 16.0),
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          shadowColor: Colors.lightBlueAccent.shade100,
                          child: MaterialButton(
                            minWidth: 200.0,
                            height: 42.0,
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                //print('Validate Success');
                                if (isConnectedToInternet == true) {
                                  login();
                                } else {
                                  Fluttertoast.showToast(
                                      msg: "Please connect to internet");
                                }
                              }
                            },
                            color: Colors.pink[400],
                            child: Text('Login',
                                style: TextStyle(color: Colors.white)),
                          ),
                        ),
                      ),

                      //----------------Google Login Button----------------
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 10.0),
                        child: Material(
                            borderRadius: BorderRadius.circular(50.0),
                            shadowColor: Colors.lightBlueAccent.shade100,
                            child: Container(
                                width: 200.0,
                                height: 42.0,
                                child: FlatButton.icon(
                                  onPressed: () {
                                    if (isConnectedToInternet == true) {
                                      handleSignIn();
                                    } else {
                                      Fluttertoast.showToast(
                                          msg: "Please connect to internet");
                                    }
                                  },
                                  label: Text(
                                    "  Log in with google",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  color: Colors.red.shade900,
                                  icon: Icon(
                                    FontAwesomeIcons.google,
                                    color: Colors.white,
                                  ),
                                ))),
                      ),

                      //----------------Create Account Button----------------
                      FlatButton(
                        onPressed: () {
                          if (isConnectedToInternet == true) {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SignUpPage()));
                          } else {
                            Fluttertoast.showToast(
                                          msg: "Please connect to internet");
                          }
                        },
                        child: Text('New user? Create an account',
                            style: TextStyle(
                                color: Colors.blue,
                                decoration: TextDecoration.underline)),
                      ),

                      SizedBox(height: 8.0)
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              child: Visibility(
                visible: isLoading ?? true,
                child: Center(
                  child: SimpleDialog(
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      children: <Widget>[
                        Center(
                          //color: Colors.white.withOpacity(0.9),
                          child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.pink[400]),
                          ),
                        ),
                      ]),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> handleSignIn() async {
    setState(() {
      isLoading = true;
    });
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;
    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    final FirebaseUser user =
        await firebaseAuth.signInWithCredential(credential);
    assert(user.email != null);
    assert(user.displayName != null);
    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await firebaseAuth.currentUser();
    assert(user.uid == currentUser.uid);

    //sharedPreferences = await SharedPreferences.getInstance();

    // setState(() {
    //   isLoading = true;
    // });

    // GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    // GoogleSignInAuthentication googleSignInAuthentication =
    //     await googleSignInAccount.authentication;
    // //FirebaseUser firebaseUser = await firebaseAuth.signInWithGoogle(idToken: null, accessToken: null)
    // AuthCredential credential = GoogleAuthProvider.getCredential(
    //     accessToken: googleSignInAuthentication.accessToken,
    //     idToken: googleSignInAuthentication.idToken);

    // FirebaseUser firebaseUser =
    //     await firebaseAuth.signInWithCredential(credential);

    if (user != null) {
      final QuerySnapshot result = await Firestore.instance
          .collection("users")
          .where("id", isEqualTo: user.uid)
          .getDocuments();
      final List<DocumentSnapshot> documents = result.documents;

      //If list is empty, means no have any user exist in the collection, it will insert into firestore
      if (documents.length == 0) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => GoogleSignUp(
                      user: user,
                    )));

        // await sharedPreferences.setString("id", user.uid);
        // await sharedPreferences.setString("username", user.displayName);
        // await sharedPreferences.setString("email", user.email);
        // await sharedPreferences.setString("photoUrl", user.photoUrl);
      } else {
        // await sharedPreferences.setString("id", documents[0]['id']);
        // await sharedPreferences.setString("username", documents[0]['username']);
        // await sharedPreferences.setString("email", documents[0]['email']);
        // await sharedPreferences.setString("photoUrl", documents[0]['photoUrl']);

        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => HomePage(
                      user: user,
                    )));

        Fluttertoast.showToast(msg: "Log in successful");
      }

      setState(() {
        if (user != null) {
          //_success = true;
          //_userID = user.uid;
          isLoading = false;
        } else {
          //_success = false;
          isLoading = false;
        }
      });
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<void> login() async {
    setState(() {
      isLoading = true;
    });
    final formState = _formKey.currentState;
    if (formState.validate()) {
      //Login to firebase
      formState.save();
      try {
        FirebaseUser user = await FirebaseAuth.instance
            .signInWithEmailAndPassword(email: _email, password: _password);

        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => HomePage(
                      user: user,
                    )));

        Fluttertoast.showToast(msg: "Welcome back");
        setState(() {
          isLoading = false;
        });
      } catch (e) {
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(msg: "Wrong email/password");
        print(e.message);
      }
    }
  }
}
