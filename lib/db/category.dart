import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:uuid/uuid.dart';

class CategoryServices {
  Firestore firestore = Firestore.instance;

  void createCategory(String name) {
    var id = Uuid();
    String categoryId = id.v1();

    firestore
        .collection('products')
        .document(categoryId)
        .setData({"name": name});
  }

  Future<List<DocumentSnapshot>> getCategories() =>
      firestore.collection('categories').getDocuments().then((snaps) {
        return snaps.documents;
      }).catchError((error) => print(error));

  Future<List<DocumentSnapshot>> getSuggestion(String suggestion) => firestore
      .collection('categories')
      .where('name', isEqualTo: suggestion)
      .getDocuments()
      .then((snapshot) => snapshot.documents);
}
