import 'package:flutter/material.dart';
import 'package:prelovedshop/pages/productDetail.dart';

class WishListProduct extends StatefulWidget {
  @override
  _WishListProductState createState() => _WishListProductState();
}

class _WishListProductState extends State<WishListProduct> {
  var wishlistProduct = [
    {
      "name": "Polo",
      "picture": "assets/products/clothes1.jpeg",
      "price": 99,
    },
    {
      "name": "Adidas Ultra Boost",
      "picture": "assets/products/shoes2.jpg",
      "price": 0,
    },
    {
      "name": "Adidas Ultra Boost",
      "picture": "assets/products/shoes2.jpg",
      "price": 0,
    },
    {
      "name": "Adidas Ultra Boost",
      "picture": "assets/products/shoes2.jpg",
      "price": 0,
    },
    {
      "name": "Adidas Ultra Boost",
      "picture": "assets/products/shoes2.jpg",
      "price": 0,
    },
    {
      "name": "Adidas Ultra Boost",
      "picture": "assets/products/shoes2.jpg",
      "price": 0,
    },
    {
      "name": "Adidas Ultra Boost",
      "picture": "assets/products/shoes2.jpg",
      "price": 0,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: wishlistProduct.length,
        itemBuilder: (context, index) {
          return SingleWishListProduct(
            productName: wishlistProduct[index]["name"],
            productPicture: wishlistProduct[index]["picture"],
            productPrice: wishlistProduct[index]["price"],
          );
        },
      ),
    );
  }
}

class SingleWishListProduct extends StatelessWidget {
  final productName;
  final productPicture;
  final productPrice;

  const SingleWishListProduct(
      {Key key, this.productName, this.productPicture, this.productPrice})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
            //Passing values to the product detail page
              builder: (context) => ProductDetail(
                productName: this.productName,
                productPrice: this.productPrice,
                productImage: this.productPicture,
              )
            )
          );
        },
        child: ListTile(
          leading: Image.asset(productPicture, width: 80.0),
          title: Text(productName),
          subtitle: Column(
            children: <Widget>[
              //Row inside the column
              Row(
                children: <Widget>[
                  Expanded(
                    child: productPrice != 0
                        ? Text(
                            "RM " + productPrice.toString(),
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        : Text(
                            "Donation Item",
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                  ),
                ],
              ),
            ],
          ),
          trailing: Column(
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.delete),
                onPressed: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}
