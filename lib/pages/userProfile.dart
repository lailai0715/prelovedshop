import 'package:flutter/material.dart';
import 'package:prelovedshop/pages/addProduct.dart';
//import 'package:prelovedshop/widgets/product.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../db/users.dart';
import '../db/product.dart';
//import 'dart:io';
import 'productDetail.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:fluttertoast/fluttertoast.dart';
import './productEdit.dart';

class UserProfile extends StatefulWidget {
  final FirebaseUser user;
  const UserProfile({Key key, this.user}) : super(key: key);

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  //TabController tabController;
  UserServices _userServices = UserServices();
  ProductServices _productServices = ProductServices();
  List<DocumentSnapshot> userProducts = <DocumentSnapshot>[];
  String _username = "";
  // String _category;
  // String _description;
  // String _title;
  // String _price;
  // String _condition;
  // String _location;
  // String _productId;
  int itemCounts;
  // File _image1;
  // File _image2;
  // File _image3;
  bool isLoading = false;
  List<String> reviews = ['review1', 'review2', 'review3'];
  bool showEdit = false;
  QuerySnapshot userWishlist;
  String _wishlistId;
  List<String> _arrWishListId = [];

  @override
  void initState() {
    super.initState();
    // TODO: implement initState
    _getUserDetail();
    //_getUserProducts();
  }

  // void _getUserProducts() async {
  //   List<DocumentSnapshot> data = Firestore.instance.collection('products').document(widget)
  //   setState(() {
  //     userProducts = data;

  //     for (int i = 0; i < userProducts.length; i++) {
  //       _title = userProducts[i].data['title'];
  //       _category = userProducts[i].data['category'];
  //       _condition = userProducts[i].data['condition'];
  //     }
  //     print(userProducts.length);
  //   });
  // }

  _getDate(Timestamp t) {
    return timeago.format(t.toDate(), locale: 'en_short');
  }

  void _getUserDetail() async {
    List<DocumentSnapshot> userdata = await _userServices.getUser(widget.user);
    setState(() {
      _username = userdata[0].data['username'].toString();
      print(userdata[0].data['username'].toString());
    });
  }

  // void showDeleteAlert(String productName, String productId) async {
  //   userWishlist = await Firestore.instance
  //       .collection('wishlist')
  //       .where('productName', isEqualTo: productName)
  //       .where('sellerId', isEqualTo: widget.user.uid)
  //       .getDocuments();

  //   // if (userWishlist.documents.length != 0) {
  //   //   _wishlistId = userWishlist.documents[0].data['id'];
  //   // }
  //   if (userWishlist.documents.length != 0) {
  //     for (int i = 0; i < userWishlist.documents.length; i++) {
  //       _arrWishListId.insert(i, userWishlist.documents[i].data['id']);
  //     }
  //   }

  //   showDialog(
  //       context: context,
  //       builder: (context) {
  //         return AlertDialog(
  //           content: Text('Are you sure you want to delete $productName ?'),
  //           actions: <Widget>[
  //             FlatButton(
  //               child: Text('CANCEL'),
  //               onPressed: () {
  //                 Navigator.of(context).pop();
  //               },
  //             ),
  //             FlatButton(
  //               child: Text('DELETE', style: TextStyle(color: Colors.red)),
  //               onPressed: () {
  //                 setState(() {
  //                   Firestore.instance
  //                       .collection('products')
  //                       .document(productId)
  //                       .delete();
  //                   for (int i = 0; i < _arrWishListId.length; i++) {
  //                     Firestore.instance
  //                         .collection('wishlist')
  //                         .document(_arrWishListId[i])
  //                         .delete();
  //                   }
  //                 });
  //                 Fluttertoast.showToast(msg: "Item is deleted");
  //                 Navigator.of(context).pop();
  //               },
  //             )
  //           ],
  //         );
  //       });
  // }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        floatingActionButton: Container(
          height: 40.0,
          width: 125.0,
          child: FloatingActionButton.extended(
            elevation: 5.0,
            icon: Icon(Icons.add_circle),
            label: Text("ADD ITEM"),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => AddProductPage(
                            user: widget.user,
                            username: _username,
                          )));
            },
            backgroundColor: Colors.pink[400],
            shape: RoundedRectangleBorder(),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.pink[400],
          title: Text('Your Profile'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.edit, color: Colors.white),
              onPressed: () {
                if (itemCounts == 0 || itemCounts == null) {
                  Fluttertoast.showToast(msg: "No product(s) to edit");
                }
                setState(() {
                  if (showEdit == false) {
                    setState(() {
                      showEdit = true;
                    });
                    Fluttertoast.showToast(msg: "Click on edit button to edit selected product");
                  } else {
                    setState(() {
                      showEdit = false;
                    });
                  }
                });
              },
            ),
          ],
        ),
        body: StreamBuilder<QuerySnapshot>(
          stream: Firestore.instance
              .collection('products')
              .where('userId', isEqualTo: widget.user.uid)
              .snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            }
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return Center(
                  child: SimpleDialog(
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      children: <Widget>[
                        Center(
                          //color: Colors.white.withOpacity(0.9),
                          child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.pink[400]),
                          ),
                        ),
                      ]),
                );
              default:
                itemCounts = snapshot.data.documents.length;
                return Column(
                  children: <Widget>[
                    Container(
                        height: 130.0,
                        color: Colors.pink[400],
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 0.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                CircleAvatar(
                                  radius: 45.0,
                                  backgroundImage:
                                      AssetImage('assets/avatar.png'),
                                ),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 10.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    _username,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20.0,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        )),
                    Container(
                      height: 40.0,
                      color: Colors.pink[400],
                      child: TabBar(
                        //controller: tabController,
                        labelColor: Colors.white,
                        tabs: <Widget>[
                          Tab(
                            text: "LISTING",
                          ),
                          Tab(
                            text: "REVIEW",
                          )
                        ],
                      ),
                    ),
                    Flexible(
                      child: TabBarView(
                        children: <Widget>[
                          //Render Listing Tab
                          snapshot.data.documents.length != 0
                              ? Container(
                                  child: GridView.builder(
                                    padding: EdgeInsets.all(5.0),
                                    itemCount: snapshot.data.documents.length,
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount: 2),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Card(
                                        child: Hero(
                                          tag: Text("Hero"),
                                          child: Material(
                                            child: InkWell(
                                              onTap: () => {
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                        //Passing values to the product detail page
                                                        builder: (context) =>
                                                            ProductDetail(
                                                              productId: snapshot
                                                                      .data
                                                                      .documents[
                                                                  index]['id'],
                                                              user: widget.user,
                                                            )))
                                              },
                                              child: Stack(children: <Widget>[
                                                Container(
                                                  height: 200.0,
                                                  width: 200.0,
                                                  child: GridTile(
                                                    footer: Container(
                                                      color: Colors.white70,
                                                      child: ListTile(
                                                        //Price condition
                                                        title: snapshot.data.documents[
                                                                        index]
                                                                    ['price'] !=
                                                                0
                                                            ? Text(
                                                                "RM" +
                                                                    snapshot
                                                                        .data
                                                                        .documents[index]
                                                                            [
                                                                            'price']
                                                                        .toString() +
                                                                    "",
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                          .pink[
                                                                      400],
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                  fontSize:
                                                                      14.0,
                                                                ),
                                                              )
                                                            : Text(
                                                                "Donation",
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                          .pink[
                                                                      400],
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                  fontSize:
                                                                      12.5,
                                                                ),
                                                              ),
                                                        // ===========================
                                                        leading: Container(
                                                            width: 75.0,
                                                            child: Padding(
                                                                padding: EdgeInsets
                                                                    .symmetric(
                                                                        vertical:
                                                                            4.0),
                                                                child: Text(
                                                                    snapshot.data
                                                                            .documents[index]
                                                                        [
                                                                        'title'],
                                                                    style:
                                                                        TextStyle(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      fontSize:
                                                                          14.0,
                                                                    )))),
                                                      ),
                                                    ),
                                                    child: Image.network(
                                                      snapshot.data
                                                              .documents[index]
                                                          ['imageUrl'][0],
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                ),
                                                showEdit == true
                                                    ? Stack(
                                                        // alignment:
                                                        //     AlignmentDirectional
                                                        //         .center,
                                                        children: <Widget>[
                                                          Align(
                                                            alignment: Alignment.topRight,
                                                            child: Container(
                                                              height: 40.0,
                                                              width: 40.0,
                                                              // color:
                                                              //     Colors.black,
                                                              child: IconButton(
                                                                onPressed: () {
                                                                  // showDeleteAlert(
                                                                  //     snapshot.data
                                                                  //             .documents[index]
                                                                  //         [
                                                                  //         'title'],
                                                                  //     snapshot.data
                                                                  //             .documents[index]
                                                                  //         ['id']);
                                                                  Navigator.of(
                                                                          context)
                                                                      .push(MaterialPageRoute(
                                                                          //Passing values to the product detail page
                                                                          builder: (context) => ProductEditPage(
                                                                                productId: snapshot.data.documents[index]['id'],
                                                                                productTitle: snapshot.data.documents[index]['title'],
                                                                                productCategory: snapshot.data.documents[index]['category'],
                                                                                productDescription: snapshot.data.documents[index]['description'],
                                                                                productLocation: snapshot.data.documents[index]['location'],
                                                                                productPrice: snapshot.data.documents[index]['price'].toString(),
                                                                                productCondition: snapshot.data.documents[index]['condition'],
                                                                                productUsername: _username,
                                                                                imageUrl1: snapshot.data.documents[index]['imageUrl'][0],
                                                                                imageUrl2: snapshot.data.documents[index]['imageUrl'][1],
                                                                                imageUrl3: snapshot.data.documents[index]['imageUrl'][2],
                                                                                user: widget.user,
                                                                              )));
                                                                },
                                                                icon: Icon(
                                                                    Icons.edit),
                                                                color:
                                                                    Colors.red,
                                                                // alignment:
                                                                //     AlignmentDirectional.topEnd,
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      )
                                                    : Container()
                                              ]),
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                )
                              : Container(
                                  child: Center(
                                    child: Text("No items found"),
                                  ),
                                ),
                          //Render Review Tab
                          StreamBuilder<QuerySnapshot>(
                              stream: Firestore.instance
                                  .collection("reviews")
                                  .where("sellerUserId",
                                      isEqualTo: widget.user.uid)
                                  .snapshots(),
                              builder: (
                                BuildContext context,
                                AsyncSnapshot<QuerySnapshot> snapshot,
                              ) {
                                if (snapshot.hasError) {
                                  return Text('Error: ${snapshot.error}');
                                }
                                switch (snapshot.connectionState) {
                                  case ConnectionState.waiting:
                                    return Center(
                                      child: SimpleDialog(
                                          elevation: 0.0,
                                          backgroundColor: Colors.transparent,
                                          children: <Widget>[
                                            Center(
                                              //color: Colors.white.withOpacity(0.9),
                                              child: CircularProgressIndicator(
                                                valueColor:
                                                    AlwaysStoppedAnimation<
                                                            Color>(
                                                        Colors.pink[400]),
                                              ),
                                            ),
                                          ]),
                                    );
                                  default:
                                    return snapshot.data.documents.length != 0
                                        ? Container(
                                            child: ListView.builder(
                                              itemCount: snapshot
                                                  .data.documents.length,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 5.0),
                                                  child: ListTile(
                                                      leading: Image.asset(
                                                          'assets/avatar.png'),
                                                      title: Text(
                                                        snapshot.data.documents[
                                                                index][
                                                            'reviewerUsername'],
                                                        style: TextStyle(
                                                          color: Colors.blue,
                                                        ),
                                                      ),
                                                      subtitle: Text(
                                                        snapshot.data.documents[
                                                            index]['comment'],
                                                        style: TextStyle(
                                                            color:
                                                                Colors.black),
                                                      ),
                                                      trailing: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .end,
                                                        children: <Widget>[
                                                          Icon(Icons.schedule,
                                                              color:
                                                                  Colors.grey),
                                                          Text(
                                                            _getDate(snapshot
                                                                            .data
                                                                            .documents[
                                                                        index][
                                                                    'timestamp'])
                                                                .toString(),
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 13.0,
                                                                color: Colors
                                                                    .grey),
                                                          )
                                                        ],
                                                      )),
                                                );
                                              },
                                            ),
                                          )
                                        : Container(
                                            child: Center(
                                              child: Text("No reviews found"),
                                            ),
                                          );
                                }
                              })
                        ],
                      ),
                    )
                  ],
                );
            }
          },
        ),
      ),
    );
  }
}
