import 'dart:io';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:prelovedshop/pages/userProfile.dart';
//import '../db/category.dart';
//import '../db/product.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';

class ProductEditPage extends StatefulWidget {
  final String productId;
  final String productTitle;
  final String productCategory;
  final String productDescription;
  final String productPrice;
  final String productCondition;
  final String productUsername;
  final String imageUrl1;
  final String imageUrl2;
  final String imageUrl3;
  final String productLocation;

  final FirebaseUser user;

  const ProductEditPage({Key key, this.productId, this.productTitle, this.productCategory, this.productDescription, this.productPrice, this.productCondition, this.productUsername, this.imageUrl1, this.imageUrl2, this.imageUrl3, this.productLocation, this.user}) : super(key: key);

  

  @override
  _ProductEditPageState createState() => _ProductEditPageState();
}

class _ProductEditPageState extends State<ProductEditPage> {
  QuerySnapshot productDetails;
  //String _username;
  String _category;
  String _description;
  String _title;
  String _price;
  String _condition;
  String _location;
  //String _productId;
  String imageUrl1;
  String imageUrl2;
  String imageUrl3;
  File _image1;
  File _image2;
  File _image3;
  bool isLoading = false;
  // TextEditingController _titleTextController = TextEditingController();
  // TextEditingController _descriptionTextController = TextEditingController();
  // TextEditingController _priceTextController = TextEditingController();
  final List<String> _allCondition = <String>['New', 'Used'];
  final List<String> _allLocation = <String>[
    'Selangor',
    'Kuala Lumpur',
    'Johor',
    'Melaka',
    'Pulau Penang',
    'Perak',
    'Pahang',
    'Perlis',
    'Kedah',
    'Kelantan',
    'Negeri Sembilan',
    'Terengganu',
    'Sarawak',
    'Sabah',
  ];
  final List<String> _allCategory = <String>[
    'Bags',
    'Clothing',
    'Shoes',
    'Books',
    'Donation'
  ];
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  Color pink = Colors.pink[400];
  List<DocumentSnapshot> categories = <DocumentSnapshot>[];
  List<DropdownMenuItem<String>> categoriesDropDownitems =
      <DropdownMenuItem<String>>[];
  // CategoryServices _categoryServices = CategoryServices();
  // ProductServices _productServices = ProductServices();
  QuerySnapshot userWishlist;
  List<String> _arrWishListId = [];
  List<String> _wishListProductIds = [];
  QuerySnapshot wishlistProducts;

  @override
  void initState() {
    super.initState();
    // TODO: implement initState
    _getProductDetails();
    //print(widget.productId);
  }

  void _getProductDetails() async {
    productDetails = await Firestore.instance
        .collection('products')
        .where('id', isEqualTo: widget.productId)
        .getDocuments();

    if (productDetails.documents.length > 0) {
      setState(() {
        _title = productDetails.documents[0].data['title'];
        _category = productDetails.documents[0].data['category'];
        _description = productDetails.documents[0].data['description'];
        _condition = productDetails.documents[0].data['condition'];
        imageUrl1 = productDetails.documents[0].data['imageUrl'][0];
        imageUrl2 = productDetails.documents[0].data['imageUrl'][1];
        imageUrl3 = productDetails.documents[0].data['imageUrl'][2];
        _location = productDetails.documents[0].data['location'];
        _price = productDetails.documents[0].data['price'].toString();
      });
    }

    // print(_title);
    //   print(_category);
    //   print(_description);
    //   print(_condition);
    //   print(imageUrl1);
    //   print(imageUrl2);
    //   print(imageUrl3);
    //   print(_location);
    //   print(_price);
  }

  void updateProduct() async {
    // print("Image1"+ _image1.toString());
    // print("Image2"+ _image2.toString());
    // print("Image3"+ _image3.toString());
    final formState = _formKey.currentState;
    if (formState.validate()) {
      setState(() {
        isLoading = true;
      });
      formState.save();
      if (_image1 == null && _image2 == null && _image3 == null) {
        //print("All images are null");
        await Firestore.instance
            .collection('products')
            .document(widget.productId)
            .updateData({
          'title': _title,
          'category': _category,
          'condition': _condition,
          'location': _location,
          'description': _description,
          'username': widget.productUsername,
          'price': _category == "Donation" ? 0.0 : double.parse(_price),
          'timestamp': Timestamp.now()
        }).catchError((onError) => print(onError));

        _formKey.currentState.reset();

        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(msg: "Product is updated");
        Navigator.pop(context);
        updateWishlist();
      } else if (_image1 != null && _image2 == null && _image3 == null) {
        //print('update image1');
        final FirebaseStorage storage = FirebaseStorage.instance;

        final String picture1 = "1${DateTime.now().toString()}.jpg";
        StorageUploadTask task1 =
            storage.ref().child(picture1).putFile(_image1);

        task1.onComplete.then((snapshot) async {
          imageUrl1 = await snapshot.ref.getDownloadURL();
          List<String> imageUrlList = [imageUrl1, imageUrl2, imageUrl3];

          await Firestore.instance
              .collection('products')
              .document(widget.productId)
              .updateData({
            'title': _title,
            'category': _category,
            'condition': _condition,
            'location': _location,
            'description': _description,
            'imageUrl': imageUrlList,
            'username': widget.productUsername,
            'price': _category == "Donation" ? 0.0 : double.parse(_price),
            'timestamp': Timestamp.now()
          }).catchError((onError) => print(onError));

          _formKey.currentState.reset();

          setState(() {
            isLoading = false;
            _image1 = null;
            _image2 = null;
            _image3 = null;
          });
          Fluttertoast.showToast(msg: "Product is updated");
          Navigator.pop(context);
          updateWishlist();
        });
      } else if (_image2 != null && _image1 == null && _image3 == null) {
        //print('update image2');
        final FirebaseStorage storage = FirebaseStorage.instance;

        final String picture2 = "1${DateTime.now().toString()}.jpg";
        StorageUploadTask task1 =
            storage.ref().child(picture2).putFile(_image2);

        task1.onComplete.then((snapshot) async {
          imageUrl2 = await snapshot.ref.getDownloadURL();
          List<String> imageUrlList = [imageUrl1, imageUrl2, imageUrl3];

          await Firestore.instance
              .collection('products')
              .document(widget.productId)
              .updateData({
            'title': _title,
            'category': _category,
            'condition': _condition,
            'location': _location,
            'description': _description,
            'imageUrl': imageUrlList,
            'username': widget.productUsername,
            'price': _category == "Donation" ? 0.0 : double.parse(_price),
            'timestamp': Timestamp.now()
          }).catchError((onError) => print(onError));

          _formKey.currentState.reset();

          setState(() {
            isLoading = false;
            _image1 = null;
            _image2 = null;
            _image3 = null;
          });
          Fluttertoast.showToast(msg: "Product is updated");
          Navigator.pop(context);
          updateWishlist();
        });
      } else if (_image3 != null && _image1 == null && _image2 == null) {
        //print('update image3');
        final FirebaseStorage storage = FirebaseStorage.instance;

        final String picture3 = "1${DateTime.now().toString()}.jpg";
        StorageUploadTask task1 =
            storage.ref().child(picture3).putFile(_image3);

        task1.onComplete.then((snapshot) async {
          imageUrl3 = await snapshot.ref.getDownloadURL();
          List<String> imageUrlList = [imageUrl1, imageUrl2, imageUrl3];

          await Firestore.instance
              .collection('products')
              .document(widget.productId)
              .updateData({
            'title': _title,
            'category': _category,
            'condition': _condition,
            'location': _location,
            'description': _description,
            'imageUrl': imageUrlList,
            'username': widget.productUsername,
            'price': _category == "Donation" ? 0.0 : double.parse(_price),
            'timestamp': Timestamp.now()
          }).catchError((onError) => print(onError));

          _formKey.currentState.reset();

          setState(() {
            isLoading = false;
            _image1 = null;
            _image2 = null;
            _image3 = null;
          });
          Fluttertoast.showToast(msg: "Product is updated");
          Navigator.pop(context);
          updateWishlist();
        });
      } else if (_image1 != null && _image2 != null && _image3 == null) {
        final FirebaseStorage storage = FirebaseStorage.instance;

        final String picture1 = "1${DateTime.now().toString()}.jpg";
        StorageUploadTask task1 =
            storage.ref().child(picture1).putFile(_image1);
        final String picture2 = "2${DateTime.now().toString()}.jpg";
        StorageUploadTask task2 =
            storage.ref().child(picture2).putFile(_image2);

        StorageTaskSnapshot snapshot1 =
            await task1.onComplete.then((snapshot) => snapshot);

        task2.onComplete.then((snapshot2) async {
          imageUrl1 = await snapshot1.ref.getDownloadURL();
          imageUrl2 = await snapshot2.ref.getDownloadURL();

          List<String> imageUrlList = [imageUrl1, imageUrl2, imageUrl3];

          await Firestore.instance
              .collection('products')
              .document(widget.productId)
              .updateData({
            'title': _title,
            'category': _category,
            'condition': _condition,
            'location': _location,
            'description': _description,
            'imageUrl': imageUrlList,
            'username': widget.productUsername,
            'price': _category == "Donation" ? 0.0 : double.parse(_price),
            'timestamp': Timestamp.now()
          }).catchError((onError) => print(onError));

          _formKey.currentState.reset();

          setState(() {
            isLoading = false;
            _image1 = null;
            _image2 = null;
            _image3 = null;
          });
          Fluttertoast.showToast(msg: "Product is uploaded");
          Navigator.pop(context);
          updateWishlist();
        });
      } else if (_image1 != null && _image3 != null && _image2 == null) {
        final FirebaseStorage storage = FirebaseStorage.instance;

        final String picture1 = "1${DateTime.now().toString()}.jpg";
        StorageUploadTask task1 =
            storage.ref().child(picture1).putFile(_image1);
        final String picture3 = "2${DateTime.now().toString()}.jpg";
        StorageUploadTask task2 =
            storage.ref().child(picture3).putFile(_image3);

        StorageTaskSnapshot snapshot1 =
            await task1.onComplete.then((snapshot) => snapshot);

        task2.onComplete.then((snapshot2) async {
          imageUrl1 = await snapshot1.ref.getDownloadURL();
          imageUrl3 = await snapshot2.ref.getDownloadURL();

          List<String> imageUrlList = [imageUrl1, imageUrl2, imageUrl3];

          await Firestore.instance
              .collection('products')
              .document(widget.productId)
              .updateData({
            'title': _title,
            'category': _category,
            'condition': _condition,
            'location': _location,
            'description': _description,
            'imageUrl': imageUrlList,
            'username': widget.productUsername,
            'price': _category == "Donation" ? 0.0 : double.parse(_price),
            'timestamp': Timestamp.now()
          }).catchError((onError) => print(onError));

          _formKey.currentState.reset();

          setState(() {
            isLoading = false;
            _image1 = null;
            _image2 = null;
            _image3 = null;
          });
          Fluttertoast.showToast(msg: "Product is uploaded");
          Navigator.pop(context);
          updateWishlist();
        });
      } else if (_image2 != null && _image3 != null && _image1 == null) {
        final FirebaseStorage storage = FirebaseStorage.instance;

        final String picture2 = "1${DateTime.now().toString()}.jpg";
        StorageUploadTask task1 =
            storage.ref().child(picture2).putFile(_image2);
        final String picture3 = "2${DateTime.now().toString()}.jpg";
        StorageUploadTask task2 =
            storage.ref().child(picture3).putFile(_image3);

        StorageTaskSnapshot snapshot1 =
            await task1.onComplete.then((snapshot) => snapshot);

        task2.onComplete.then((snapshot2) async {
          imageUrl2 = await snapshot1.ref.getDownloadURL();
          imageUrl3 = await snapshot2.ref.getDownloadURL();

          List<String> imageUrlList = [imageUrl1, imageUrl2, imageUrl3];

          await Firestore.instance
              .collection('products')
              .document(widget.productId)
              .updateData({
            'title': _title,
            'category': _category,
            'condition': _condition,
            'location': _location,
            'description': _description,
            'imageUrl': imageUrlList,
            'username': widget.productUsername,
            'price': _category == "Donation" ? 0.0 : double.parse(_price),
            'timestamp': Timestamp.now()
          }).catchError((onError) => print(onError));

          _formKey.currentState.reset();

          setState(() {
            isLoading = false;
            _image1 = null;
            _image2 = null;
            _image3 = null;
          });
          Fluttertoast.showToast(msg: "Product is uploaded");
          Navigator.pop(context);
          updateWishlist();
        });
      } else if (_image1 != null && _image2 != null && _image3 != null) {
        //print("coming here");
        final FirebaseStorage storage = FirebaseStorage.instance;

        final String picture1 = "1${DateTime.now().toString()}.jpg";
        StorageUploadTask task1 =
            storage.ref().child(picture1).putFile(_image1);
        final String picture2 = "2${DateTime.now().toString()}.jpg";
        StorageUploadTask task2 =
            storage.ref().child(picture2).putFile(_image2);
        final String picture3 = "3${DateTime.now().toString()}.jpg";
        StorageUploadTask task3 =
            storage.ref().child(picture3).putFile(_image3);

        StorageTaskSnapshot snapshot1 =
            await task1.onComplete.then((snapshot) => snapshot);
        StorageTaskSnapshot snapshot2 =
            await task2.onComplete.then((snapshot) => snapshot);
        //print("Coming over hereeeeeeeee");
        //StorageTaskSnapshot snapshot3 = await task3.onComplete.then((snapshot) => snapshot);

        task3.onComplete.then((snapshot3) async {
          imageUrl1 = await snapshot1.ref.getDownloadURL();
          imageUrl2 = await snapshot2.ref.getDownloadURL();
          imageUrl3 = await snapshot3.ref.getDownloadURL();

          List<String> imageUrlList = [imageUrl1, imageUrl2, imageUrl3];

          await Firestore.instance
              .collection('products')
              .document(widget.productId)
              .updateData({
            'title': _title,
            'category': _category,
            'condition': _condition,
            'location': _location,
            'description': _description,
            'imageUrl': imageUrlList,
            'username': widget.productUsername,
            'price': _category == "Donation" ? 0.0 : double.parse(_price),
            'timestamp': Timestamp.now()
          }).catchError((onError) => print(onError));

          _formKey.currentState.reset();

          setState(() {
            isLoading = false;
            _image1 = null;
            _image2 = null;
            _image3 = null;
          });
          Fluttertoast.showToast(msg: "Product is uploaded");
          Navigator.pop(context);
          updateWishlist();
        });
      }
    }
  }

  void updateWishlist() async {
    wishlistProducts = await Firestore.instance
        .collection('wishlist')
        .where('productId', isEqualTo: widget.productId)
        .getDocuments();

    if (wishlistProducts.documents.length != 0) {
      for (int i = 0; i < wishlistProducts.documents.length; i++) {
        _wishListProductIds.insert(i, wishlistProducts.documents[i].data['id']);
      }
    }
    //print("WISHLISTHPRODUCTIDSLENGTH " + _wishListProductIds.length.toString());
    for (int i = 0; i < _wishListProductIds.length; i++) {
      await Firestore.instance
          .collection('wishlist')
          .document(_wishListProductIds[i])
          .updateData({
        'productName': _title,
        'productImage': imageUrl1,
        'productPrice': _category == "Donation" ? 0.0 : double.parse(_price),
      }).catchError((onError) => print(onError));
    }
  }

  void _selectImage(Future<File> pickImage, int imageNo) async {
    File tempImg = await pickImage;
    switch (imageNo) {
      case 1:
        setState(() {
          _image1 = tempImg;
          tempImg = null;
        });
        break;
      case 2:
        setState(() {
          _image2 = tempImg;
          tempImg = null;
        });
        break;
      case 3:
        setState(() {
          _image3 = tempImg;
          tempImg = null;
        });
        break;
    }
  }

  void showDeleteAlert(String productName, String productId) async {
    userWishlist = await Firestore.instance
        .collection('wishlist')
        .where('productName', isEqualTo: productName)
        .where('sellerId', isEqualTo: widget.user.uid)
        .getDocuments();

    // if (userWishlist.documents.length != 0) {
    //   _wishlistId = userWishlist.documents[0].data['id'];
    // }
    if (userWishlist.documents.length != 0) {
      for (int i = 0; i < userWishlist.documents.length; i++) {
        _arrWishListId.insert(i, userWishlist.documents[i].data['id']);
      }
    }

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Text('Are you sure you want to delete $productName ?'),
            actions: <Widget>[
              FlatButton(
                child: Text('CANCEL'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text('DELETE', style: TextStyle(color: Colors.red)),
                onPressed: () {
                  setState(() {
                    Firestore.instance
                        .collection('products')
                        .document(productId)
                        .delete();
                    for (int i = 0; i < _arrWishListId.length; i++) {
                      Firestore.instance
                          .collection('wishlist')
                          .document(_arrWishListId[i])
                          .delete();
                    }
                  });
                  Fluttertoast.showToast(msg: "Item is deleted");
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => UserProfile(
                                user: widget.user,
                              )));
                },
              )
            ],
          );
        });
  }

  Widget _displayChild1() {
    if (_image1 == null) {
      return
          // Padding(
          //   padding: EdgeInsets.fromLTRB(14, 50, 14, 50),
          //   child: Icon(
          //     Icons.add,
          //     color: Colors.grey,
          //   ),
          // );
          Image.network(
        widget.imageUrl1,
        fit: BoxFit.fill,
        width: double.infinity,
      );
    } else {
      return Image.file(
        _image1,
        fit: BoxFit.fill,
        width: double.infinity,
      );
    }
  }

  Widget _displayChild2() {
    if (_image2 == null) {
      return
          // Padding(
          //   padding: EdgeInsets.fromLTRB(14, 50, 14, 50),
          //   child: Icon(
          //     Icons.add,
          //     color: Colors.grey,
          //   ),
          // );
          Image.network(
        widget.imageUrl2,
        fit: BoxFit.fill,
        width: double.infinity,
      );
    } else {
      return Image.file(
        _image2,
        fit: BoxFit.fill,
        width: double.infinity,
      );
    }
  }

  Widget _displayChild3() {
    if (_image3 == null) {
      return
          // Padding(
          //   padding: EdgeInsets.fromLTRB(14, 50, 14, 50),
          //   child: Icon(
          //     Icons.add,
          //     color: Colors.grey,
          //   ),
          // );
          Image.network(
        widget.imageUrl3,
        fit: BoxFit.fill,
        width: double.infinity,
      );
    } else {
      return Image.file(
        _image3,
        fit: BoxFit.fill,
        width: double.infinity,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.pink[400],
        title: Text('Update Item'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.done, color: Colors.white),
            onPressed: () {
              updateProduct();
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDeleteAlert(widget.productTitle, widget.productId);
        },
        child: Icon(Icons.delete),
        backgroundColor: Colors.pink[400],
      ),
      body: Stack(children: <Widget>[
        DropdownButtonHideUnderline(
          child: SafeArea(
            top: false,
            bottom: false,
            child: Form(
              key: _formKey,
              child: SafeArea(
                child: ListView(
                  padding: EdgeInsets.all(16.0),
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 0.0, bottom: 10.0),
                          child: Text(
                            "Insert at least 3 images",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(right: 4.0),
                            child: OutlineButton(
                              borderSide: BorderSide(
                                  color: Colors.grey.withOpacity(0.8),
                                  width: 1.0),
                              onPressed: () {
                                _selectImage(
                                    ImagePicker.pickImage(
                                        source: ImageSource.gallery),
                                    1);
                              },
                              child: _displayChild1(),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 1.0),
                            child: OutlineButton(
                              borderSide: BorderSide(
                                  color: Colors.grey.withOpacity(0.8),
                                  width: 1.0),
                              onPressed: () {
                                _selectImage(
                                    ImagePicker.pickImage(
                                        source: ImageSource.gallery),
                                    2);
                              },
                              child: _displayChild2(),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(left: 4.0),
                            child: OutlineButton(
                              borderSide: BorderSide(
                                  color: Colors.grey.withOpacity(0.8),
                                  width: 1.0),
                              onPressed: () {
                                _selectImage(
                                    ImagePicker.pickImage(
                                        source: ImageSource.gallery),
                                    3);
                              },
                              child: _displayChild3(),
                            ),
                          ),
                        )
                      ],
                    ),
                    Container(
                      //color: Colors.grey,
                      height: 60.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(mainAxisAlignment: MainAxisAlignment.center,
                              //crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(left: 0.0),
                                  child: Text(
                                    "Category",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20.0,
                                    ),
                                  ),
                                ),
                              ]),
                          Column(mainAxisAlignment: MainAxisAlignment.center,
                              //crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(left: 0.0),
                                  child: DropdownButton<String>(
                                    value: _category,
                                    onChanged: (String newValue) {
                                      setState(() {
                                        _category = newValue;
                                      });
                                    },
                                    items: _allCategory
                                        .map<DropdownMenuItem<String>>(
                                            (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                    //items: categoriesDropDownitems,
                                    //onChanged: changeSelectedCategory,
                                  ),
                                ),
                              ]),
                        ],
                      ),
                    ),
                    Container(
                      //color: Colors.grey,
                      height: 60.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(left: 0.0),
                            child: Text(
                              "Condition",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20.0,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 0.0),
                            child: DropdownButton<String>(
                              value: _condition,
                              onChanged: (String newValue) {
                                setState(() {
                                  _condition = newValue;
                                });
                              },
                              items: _allCondition
                                  .map<DropdownMenuItem<String>>(
                                      (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              // items: categoriesDropDownitems,
                              // onChanged: changeSelectedCategory,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      //color: Colors.grey,
                      height: 60.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(left: 0.0),
                            child: Text(
                              "Location",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20.0,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 0.0),
                            child: DropdownButton<String>(
                              value: _location,
                              onChanged: (String newValue) {
                                setState(() {
                                  _location = newValue;
                                });
                              },
                              items: _allLocation.map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              // items: categoriesDropDownitems,
                              // onChanged: changeSelectedCategory,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(),
                    InputDecorator(
                      decoration: const InputDecoration(
                        labelText: 'Product Title',
                        labelStyle: TextStyle(
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                        hintText: 'Enter your title here',
                        contentPadding: EdgeInsets.zero,
                      ),
                      child: Padding(
                        padding: EdgeInsets.only(top: 10.0),
                        child: TextFormField(
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Enter your title here',
                            //helperText: 'Keep it short, this is just a demo.',
                            labelText: 'Enter your title here',
                          ),
                          initialValue: widget.productTitle,
                          maxLines: 2,
                          //controller: _titleTextController,
                          onSaved: (input) => _title = input,
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Title can not be empty';
                            } else if (value.length > 30) {
                              return 'Product title can\'t have more than 30 letters ';
                            }
                          },
                        ),
                      ),
                    ),
                    Divider(),
                    SizedBox(
                      height: 10.0,
                    ),
                    InputDecorator(
                      decoration: const InputDecoration(
                        labelText: 'Description',
                        labelStyle: TextStyle(
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                        hintText: 'Enter your description here',
                        contentPadding: EdgeInsets.zero,
                      ),
                      child: Container(
                        //color: Colors.grey,
                        height: 220.0,
                        child: ListView(children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 10.0),
                            child: TextFormField(
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: 'Enter your description here',
                                  //helperText: 'Keep it short, this is just a demo.',
                                  labelText: 'Enter your description here',
                                  labelStyle: TextStyle()),
                              initialValue: widget.productDescription,
                              maxLines: 20,
                              //controller: _descriptionTextController,
                              onSaved: (input) => _description = input,
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Description can not be empty';
                                }
                              },
                            ),
                          ),
                        ]),
                      ),
                    ),
                    Divider(),
                    _category != 'Donation'
                        ? Row(
                            children: <Widget>[
                              Expanded(
                                  flex: 1,
                                  child: Text(
                                    "Price",
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold),
                                  )),
                              Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: EdgeInsets.only(),
                                  child: TextFormField(
                                    initialValue: widget.productPrice,
                                    keyboardType: TextInputType.number,
                                    //controller: _priceTextController,
                                    onSaved: (input) => _price = input,
                                    style: TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                        labelText: 'Set your price here in RM'),
                                    validator: (String value) {
                                      if (value.isEmpty) {
                                        return 'Price can not be empty';
                                      }
                                    },
                                  ),
                                ),
                              ),
                              Divider()
                            ],
                          )
                        : Divider(),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          child: Visibility(
            visible: isLoading ?? true,
            child: Center(
              child: SimpleDialog(
                  elevation: 0.0,
                  backgroundColor: Colors.transparent,
                  children: <Widget>[
                    Center(
                      //color: Colors.white.withOpacity(0.9),
                      child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.pink[400]),
                      ),
                    ),
                  ]),
            ),
          ),
        )
      ]),
    );
  }
}
