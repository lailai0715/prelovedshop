import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './productDetail.dart';
import 'package:fluttertoast/fluttertoast.dart';
//import 'package:uuid/uuid.dart';
import './searchPage.dart';
//import '../widgets/wishlistProduct.dart';

class WishList extends StatefulWidget {
  final FirebaseUser user;

  const WishList({Key key, this.user}) : super(key: key);
  @override
  _WishListState createState() => _WishListState();
}

class _WishListState extends State<WishList> {
  QuerySnapshot userWishlist;
  String _wishlistId;

  void showDeleteAlert(String productName, String productUserId) async {
    userWishlist = await Firestore.instance
        .collection('wishlist')
        .where('productName', isEqualTo: productName)
        .where('userId', isEqualTo: widget.user.uid)
        .getDocuments();

    if (userWishlist.documents.length != 0) {
      _wishlistId = userWishlist.documents[0].data['id'];
    }
    //print(_wishlistId);

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Text(
                'Are you sure you want to delete $productName from the wishlist?'),
            actions: <Widget>[
              FlatButton(
                child: Text('CANCEL'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text('DELETE', style: TextStyle(color: Colors.red)),
                onPressed: () {
                  setState(() {
                    Firestore.instance
                        .collection('wishlist')
                        .document(_wishlistId)
                        .delete();
                  });
                  Fluttertoast.showToast(
                      msg: "Item has been remove from the wishlist");
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.1,
          backgroundColor: Colors.pink[400],
          title: Text('Wish List'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search, color: Colors.white),
              onPressed: () {
                 Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SearchPage(user: widget.user)));
              },
            ),
          ],
        ),
        // bottomNavigationBar: Container(
        //   height: 60.0,
        //   color: Colors.white,
        //   child: Row(
        //     children: <Widget>[
        //       Expanded(
        //         flex: 2,
        //         child: ListTile(
        //           title: Text("Total: "),
        //           subtitle: Text("RM 200"),
        //         ),
        //       ),
        //       Expanded(
        //         flex: 2,
        //         child: Padding(
        //           padding: EdgeInsets.only(right: 10.0),
        //           child: MaterialButton(
        //             onPressed: () {},
        //             child: Text(
        //               "Checkout",
        //               style: TextStyle(color: Colors.white),
        //             ),
        //             color: Colors.pink[400],
        //           ),
        //         ),
        //       )
        //     ],
        //   ),
        // ),
        body: StreamBuilder<QuerySnapshot>(
          stream: Firestore.instance
              .collection('wishlist')
              .where('userId', isEqualTo: widget.user.uid)
              .snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            }
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return Center(
                  child: SimpleDialog(
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      children: <Widget>[
                        Center(
                          //color: Colors.white.withOpacity(0.9),
                          child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.pink[400]),
                          ),
                        ),
                      ]),
                );
              default:
                return snapshot.data.documents.length != 0
                    ? Container(
                        child: ListView.builder(
                          itemCount: snapshot.data.documents.length,
                          itemBuilder: (context, index) {
                            return Card(
                              child: InkWell(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      //Passing values to the product detail page
                                      builder: (context) => ProductDetail(
                                            productId: snapshot.data
                                                .documents[index]['productId'],
                                            user: widget.user,
                                          )));
                                },
                                child: ListTile(
                                  leading: Image.network(
                                      snapshot.data.documents[index]
                                          ['productImage'],
                                      width: 80.0),
                                  title: Text(snapshot.data.documents[index]
                                      ['productName']),
                                  subtitle: Column(
                                    children: <Widget>[
                                      //Row inside the column
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: snapshot.data
                                                            .documents[index]
                                                        ['productPrice'] !=
                                                    0
                                                ? Text(
                                                    "RM" +
                                                        snapshot
                                                            .data
                                                            .documents[index]
                                                                ['productPrice']
                                                            .toString() +
                                                        "0",
                                                    style: TextStyle(
                                                      color: Colors.red,
                                                      fontSize: 15.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  )
                                                : Text(
                                                    "Donation Item",
                                                    style: TextStyle(
                                                      color: Colors.red,
                                                      fontSize: 15.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  trailing: Column(
                                    children: <Widget>[
                                      IconButton(
                                        icon: Icon(Icons.delete),
                                        onPressed: () {
                                          showDeleteAlert(
                                              snapshot.data.documents[index]
                                                  ['productName'],
                                              snapshot.data.documents[index]
                                                  ['userId']);
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      )
                    : Container(
                        child: Center(
                          child: Text("Your wishlist is empty"),
                        ),
                      );
            }
          },
        ));
  }
}
