import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:uuid/uuid.dart';
import 'package:firebase_auth/firebase_auth.dart';

class ReviewServices{
  Firestore firestore = Firestore.instance;

 void addReview(String comment,
  FirebaseUser reviewer, String sellerUserid, String reviewerUsername) async {
    var rid = Uuid();
    String reviewId = rid.v1();
   
   await firestore.collection('reviews').document(reviewId).setData({
      "id": reviewId,
      "comment": comment,
      "reviewerUserId": reviewer.uid,
      "reviewerUsername": reviewerUsername,
      "sellerUserId": sellerUserid,
      "timestamp": Timestamp.now(),

    }).catchError((error)=>print(error));
 }
}