import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
//import 'package:prelovedshop/widgets/horizontalListView.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'productDetail.dart';
import './searchPage.dart';
import './wishlist.dart';

class ProductListingPage extends StatefulWidget {
  final FirebaseUser user;
  final String
      category; // user pass the category type on show all the specific category type products

  const ProductListingPage({Key key, this.user, this.category})
      : super(key: key);

  @override
  _ProductListingState createState() => _ProductListingState();
}

class _ProductListingState extends State<ProductListingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //elevation: 0.0,
        backgroundColor: Colors.pink[400],
        title: Text(widget.category + ""),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.white),
            onPressed: () {
              Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SearchPage(user: widget.user)));
            },
          ),
          IconButton(
            icon: Icon(Icons.favorite_border, color: Colors.white),
            onPressed: () {
              Navigator.push(context,
                    MaterialPageRoute(builder: (context) => WishList(user: widget.user)));
            },
          ),
        ],
      ),
      body: StreamBuilder<QuerySnapshot>(
          stream:
              Firestore.instance.collection('products')
              .where('category', isEqualTo: widget.category)
              .snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            }
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return Center(
                  child: SimpleDialog(
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      children: <Widget>[
                        Center(
                          //color: Colors.white.withOpacity(0.9),
                          child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.pink[400]),
                          ),
                        ),
                      ]),
                );
              default: 
                return snapshot.data.documents.length != 0 ?
                 Container(
                  child: GridView.builder(
                    itemCount: snapshot.data.documents.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2),
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        child: Hero(
                          tag: Text("Hero"),
                          child: Material(
                            child: InkWell(
                              onTap: () => {
                                Navigator.of(context).push(MaterialPageRoute(
                                    //Passing values to the product detail page
                                    builder: (context) => ProductDetail(
                                          productId: snapshot
                                              .data.documents[index]['id'],
                                          user: widget.user,
                                        )))
                              },
                              child: GridTile(
                                footer: Container(
                                  color: Colors.white70,
                                  child: ListTile(
                                    //Price condition
                                    title: snapshot.data.documents[index]
                                                ['price'] != 0
                                        ? Text(
                                            "RM " +
                                                snapshot.data
                                                    .documents[index]['price']
                                                    .toString()+"0",
                                            style: TextStyle(
                                              color: Colors.pink[400],
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14.0,
                                            ),
                                          )
                                        : Text(
                                            "Donation",
                                            style: TextStyle(
                                              color: Colors.pink[400],
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12.5,
                                            ),
                                          ),
                                    // ===========================
                                    leading: Container(
                                        width: 75.0,
                                        child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 4.0),
                                            child: Text(
                                                snapshot.data.documents[index]
                                                    ['title'],
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 14.0,
                                                )))),
                                  ),
                                ),
                                child: Image.network(
                                  snapshot.data.documents[index]['imageUrl'][0],
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ) : Container(
                  child: Center(
                    child: Text("No items found"),
                  ),
                );
            }
          }),
    );
  }
}
