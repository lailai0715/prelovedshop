import 'package:flutter/material.dart';
//import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:algolia/algolia.dart';
import 'package:fluttertoast/fluttertoast.dart';
import './productDetail.dart';

class SearchPage extends StatefulWidget {
  final FirebaseUser user;

  const SearchPage({Key key, this.user}) : super(key: key);
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  TextEditingController _searchText = TextEditingController(text: "");
  List<AlgoliaObjectSnapshot> _results = [];
  bool _searching = false;

  _search() async {
    setState(() {
      _searching = true;
    });

    Algolia algolia = Algolia.init(
      applicationId: 'E2IDYCXITV',
      apiKey: 'c9a856e55e2670a49c961ee32a11cdbd',
    );

    AlgoliaQuery query = algolia.instance.index('products');
    query = query.search(_searchText.text);

    _results = (await query.getObjects()).hits;
    //print(_results);

    setState(() {
      _searching = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pink[400],
        title: TextField(
          style: TextStyle(color: Colors.white),
          autofocus: true,
          cursorColor: Colors.white,
          controller: _searchText,
          decoration: InputDecoration(
              hintStyle: TextStyle(color: Colors.white),
              hintText: "Search products here...",
              border: InputBorder.none),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.white),
            onPressed: () {
              if (_searchText.text == "" || _searchText.text == null) {
                Fluttertoast.showToast(msg: "Please enter at least 1 word to search");
              } else {
                _search();
              }
            },
          ),
        ],
      ),
      body: Container(
        padding: const EdgeInsets.only(top: 0.0, left: 13.0, right: 13.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //Text("Search"),

            // Padding(
            //   padding: EdgeInsets.only(bottom: 5.0),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     children: <Widget>[
            //       Container(
            //         width: 250.0,
            //         child: TextField(
            //           controller: _searchText,
            //           decoration:
            //               InputDecoration(hintText: "Search products here..."),
            //         ),
            //       ),
            //       IconButton(
            //         icon: Icon(Icons.search, color: Colors.grey),
            //         onPressed: () {
            //           _search();
            //         },
            //       ),
            //     ],
            //   ),
            // ),
            Expanded(
              child: _searching == true
                  ? Center(
                      child: Text("Searching, please wait..."),
                    )
                  : _results.length == 0
                      ? Center(
                          child: Text("No results found."),
                        )
                      :
                      // ListView.builder(
                      //     itemCount: _results.length,
                      //     itemBuilder: (BuildContext ctx, int index) {
                      //       AlgoliaObjectSnapshot snap = _results[index];

                      //       return ListTile(
                      //         leading: CircleAvatar(
                      //           child: Text(
                      //             (index + 1).toString(),
                      //           ),
                      //         ),
                      //         title: Text(snap.data["title"]),
                      //         subtitle: Text(snap.data["price"].toString()),
                      //       );
                      //     },
                      //   ),
                      // ListView.builder(
                      //     itemCount: _results.length,
                      //     itemBuilder: (context, index) {
                      //       AlgoliaObjectSnapshot snap = _results[index];
                      //       return Card(
                      //         child: InkWell(
                      //           onTap: () {
                      //             Navigator.of(context).push(MaterialPageRoute(
                      //                 //Passing values to the product detail page
                      //                 builder: (context) => ProductDetail(
                      //                       productId: snap.data['id'],
                      //                       user: widget.user,
                      //                     )));
                      //           },
                      //           child: ListTile(
                      //             leading: Image.network(
                      //                 snap.data['imageUrl'][0],
                      //                 width: 80.0),

                      //             title: Text(snap.data['title']),
                      //             subtitle: Column(
                      //               children: <Widget>[
                      //                 //Row inside the column
                      //                 Row(
                      //                   children: <Widget>[
                      //                     Expanded(
                      //                       child: snap.data['price'] != 0
                      //                           ? Text(
                      //                               "RM" +
                      //                                   snap.data['price']
                      //                                       .toString() +
                      //                                   ".00",
                      //                               style: TextStyle(
                      //                                 color: Colors.red,
                      //                                 fontSize: 15.0,
                      //                                 fontWeight:
                      //                                     FontWeight.bold,
                      //                               ),
                      //                             )
                      //                           : Text(
                      //                               "Donation Item",
                      //                               style: TextStyle(
                      //                                 color: Colors.red,
                      //                                 fontSize: 15.0,
                      //                                 fontWeight:
                      //                                     FontWeight.bold,
                      //                               ),
                      //                             ),
                      //                     ),
                      //                   ],
                      //                 ),
                      //               ],
                      //             ),
                      //             // trailing: Column(
                      //             //   children: <Widget>[
                      //             //     IconButton(
                      //             //       icon: Icon(Icons.delete),
                      //             //       onPressed: () {},
                      //             //     ),
                      //             //   ],
                      //             // ),
                      //           ),
                      //         ),
                      //       );
                      //     },
                      //   ),
                      GridView.builder(
                          itemCount: _results.length,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2),
                          itemBuilder: (BuildContext context, int index) {
                            AlgoliaObjectSnapshot snap = _results[index];
                            return Card(
                              child: Hero(
                                tag: Text("Hero"),
                                child: Material(
                                  child: InkWell(
                                    onTap: () => {
                                      Navigator.of(context).push(MaterialPageRoute(
                                          //Passing values to the product detail page
                                          builder: (context) => ProductDetail(
                                                productId: snap.data['id'],
                                                user: widget.user,
                                              )))
                                    },
                                    child: GridTile(
                                      footer: Container(
                                        color: Colors.white70,
                                        child: ListTile(
                                          //Price condition
                                          title: snap.data['price'] != 0
                                              ? Text(
                                                  "RM" +
                                                      snap.data['price']
                                                          .toString() +
                                                      "",
                                                  style: TextStyle(
                                                    color: Colors.pink[400],
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 14.0,
                                                  ),
                                                )
                                              : Text(
                                                  "Donation",
                                                  style: TextStyle(
                                                    color: Colors.pink[400],
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 12.5,
                                                  ),
                                                ),
                                          // ===========================
                                          leading: Container(
                                              width: 75.0,
                                              child: Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 4.0),
                                                  child:
                                                      Text(snap.data['title'],
                                                          style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 14.0,
                                                          )))),
                                        ),
                                      ),
                                      child: Image.network(
                                        snap.data['imageUrl'][0],
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
            ),
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
