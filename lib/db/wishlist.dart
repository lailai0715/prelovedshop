import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:uuid/uuid.dart';
import 'package:firebase_auth/firebase_auth.dart';

class WishListServices{
  Firestore firestore = Firestore.instance;

 void addProductToWishlist(FirebaseUser user, String sellerId, String productId, 
 String productName, double productPrice, String productImage){
    var wid = Uuid();
    String wishlistId = wid.v1();
   
   
    
   
   firestore.collection('wishlist').document(wishlistId).setData({
      "id": wishlistId,
      "userId": user.uid,
      "sellerId": sellerId,
      "productId": productId,
      "productName": productName,
      "productPrice": productPrice,
      "productImage": productImage,
      "timestamp": Timestamp.now(),

    }).catchError((error)=>print(error));
 }
}