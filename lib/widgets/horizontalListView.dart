import 'package:flutter/material.dart';
import 'package:prelovedshop/pages/productListing.dart';
import 'package:firebase_auth/firebase_auth.dart';

class HorizontalListView extends StatefulWidget {
  final FirebaseUser user;

  const HorizontalListView({
    Key key,
    this.user
  }): super(key: key);

  @override
  _HorizontalListViewState createState() => _HorizontalListViewState();
}

class _HorizontalListViewState extends State<HorizontalListView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 75.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Category(
           imageLocation: 'assets/donation-icon.png',
           imageCaption: 'Donation',
           category: 'Donation',
           user: widget.user,
         ),
         Category(
           imageLocation: 'assets/dress.png',
           imageCaption: 'Clothing',
           category: 'Clothing',
           user: widget.user,
         ),
          Category(
           imageLocation: 'assets/shoe.png',
           imageCaption: 'Shoes',
           category: 'Shoes',
           user: widget.user,
         ),
          Category(
           imageLocation: 'assets/notebook.png',
           imageCaption: 'Books',
           category: 'Books',
           user: widget.user,
         ),
         Category(
           imageLocation: 'assets/backpack.png',
           imageCaption: 'Bags',
           category: 'Bags',
           user: widget.user,
         ),
        ],
      ),
    );
  }
}

class Category extends StatelessWidget {
  final String imageLocation;
  final String imageCaption;
  final String category;
  final FirebaseUser user;

  const Category({Key key, this.imageLocation, this.imageCaption, this.category, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(2.0),
      child: InkWell(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(
            builder: (context) => ProductListingPage(
              category: category,
              user: user,
            )
          ));
        },
        child: Container(
          width: 100.0,
          child: ListTile(
            title: Image.asset(imageLocation, width: 80.0, height: 50.0,),
            subtitle: Text(imageCaption, textAlign: TextAlign.center,),
          ),
        ),
      ),
    );
  }
}