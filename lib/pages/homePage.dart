import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:prelovedshop/db/users.dart';
import 'package:prelovedshop/pages/userProfile.dart';
import 'package:prelovedshop/pages/wishlist.dart';
import 'package:prelovedshop/widgets/imageCarousel.dart';
//import 'package:prelovedshop/widgets/product.dart';
import '../widgets/horizontalListView.dart';
//import '../widgets/itemsListingHome.dart';
import './loginPage.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'productDetail.dart';
import './searchPage.dart';
import './editUserProfile.dart';

class HomePage extends StatefulWidget {
  final FirebaseUser user;

  const HomePage({Key key, this.user}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  UserServices _userServices = UserServices();
  String _username = "";
  String _phoneNumber;

  @override
  void initState() {
    super.initState();
    
    _getUserDetail();
  }

  void _getUserDetail() async {
    List<DocumentSnapshot> userdata = await _userServices.getUser(widget.user);
    setState(() {
      _username = userdata[0].data['username'].toString();
      _phoneNumber = userdata[0].data['phone_number'].toString();
      //print(userdata[0].data['username'].toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Login Demo',
      theme: ThemeData(
          //primarySwatch: Colors.pink,
          ),
      home: Scaffold(
        appBar: AppBar(
          //elevation: 0.0,
          backgroundColor: Colors.pink[400],
          title: Text('Preloved Shop'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search, color: Colors.white),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SearchPage(user: widget.user)));
              },
            ),
            IconButton(
              icon: Icon(Icons.favorite_border, color: Colors.white),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => WishList(user: widget.user)));
              },
            ),
          ],
        ),
        drawer: Container(
          width: 250.0,
          child: Drawer(
            //elevation: 5.0,
            child: ListView(
              children: <Widget>[
                UserAccountsDrawerHeader(
                  decoration: BoxDecoration(color: Colors.pink[400]),
                  accountName: _username != null
                      ? Text(_username)
                      : Text(widget.user.displayName),
                  accountEmail: Text(widget.user.email),
                  currentAccountPicture: GestureDetector(
                    child: CircleAvatar(
                      backgroundImage: AssetImage('assets/avatar.png'),
                      //backgroundColor: Colors.grey,
                      //child: Icon(Icons.person, color: Colors.white),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {},
                  child: ListTile(
                    title: Text('Home Page'),
                    leading: Icon(
                      Icons.home,
                      color: Colors.pink[400],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                UserProfile(user: widget.user)));
                  },
                  child: ListTile(
                    title: Text('Manage products'),
                    leading: Icon(
                      Icons.person,
                      color: Colors.pink[400],
                    ),
                  ),
                ),
                //divider
                Divider(),
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EditUserProfilePage(
                                  user: widget.user,
                                  phoneNumber: _phoneNumber,
                                  userName: _username,
                                )));
                  },
                  child: ListTile(
                    title: Text('Edit User Profile'),
                    leading: Icon(
                      Icons.settings,
                      color: Colors.grey,
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    FirebaseAuth.instance.signOut().then((value) {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) => LoginPage()));
                    });
                    Fluttertoast.showToast(msg: "Log out successful");
                  },
                  child: ListTile(
                    title: Text('Log out'),
                    leading: Icon(
                      Icons.transit_enterexit,
                      color: Colors.red,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        body: Column(
          children: <Widget>[
            //Insert image carousel Widget
            ImageCarousel(),

            Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                'CATEGORIES',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
              ),
            ),

            //Horizontal List View
            HorizontalListView(user: widget.user),

            Padding(
              padding: EdgeInsets.all(10.0),
              child: Text(
                'FEATURE PRODUCTS',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
              ),
            ),

            //Listing feature product
            Flexible(
              child: StreamBuilder<QuerySnapshot>(
                  stream: Firestore.instance
                      .collection('products')
                      .limit(10)
                      .snapshots(),
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (snapshot.hasError) {
                      return Text('Error: ${snapshot.error}');
                    }
                    switch (snapshot.connectionState) {
                      case ConnectionState.waiting:
                        return Center(
                          child: SimpleDialog(
                              elevation: 0.0,
                              backgroundColor: Colors.transparent,
                              children: <Widget>[
                                Center(
                                  //color: Colors.white.withOpacity(0.9),
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.pink[400]),
                                  ),
                                ),
                              ]),
                        );
                      default:
                        return Container(
                          child: SafeArea(
                            child: GridView.builder(
                              padding: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 5.0),
                              itemCount: snapshot.data.documents.length,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2),
                              itemBuilder: (BuildContext context, int index) {
                                return Card(
                                  child: Hero(
                                    tag: Text("Hero"),
                                    child: Material(
                                      child: InkWell(
                                        onTap: () => {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  //Passing values to the product detail page
                                                  builder: (context) =>
                                                      ProductDetail(
                                                        productId: snapshot
                                                                .data.documents[
                                                            index]['id'],
                                                        user: widget.user,
                                                      )))
                                        },
                                        child: GridTile(
                                          footer: Container(
                                            color: Colors.white70,
                                            child: ListTile(
                                              //Price condition
                                              title: snapshot.data
                                                              .documents[index]
                                                          ['price'] !=
                                                      0
                                                  ? Text(
                                                      "RM" +
                                                          snapshot
                                                              .data
                                                              .documents[index]
                                                                  ['price']
                                                              .toString() +
                                                          "0",
                                                      style: TextStyle(
                                                        color: Colors.pink[400],
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 14.0,
                                                      ),
                                                    )
                                                  : Text(
                                                      "Donation",
                                                      style: TextStyle(
                                                        color: Colors.pink[400],
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 12.5,
                                                      ),
                                                    ),
                                              // ===========================
                                              leading: Container(
                                                  width: 75.0,
                                                  child: Padding(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              vertical: 4.0),
                                                      child: Text(
                                                          snapshot.data
                                                                  .documents[
                                                              index]['title'],
                                                          style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 14.0,
                                                          )))),
                                            ),
                                          ),
                                          child: Image.network(
                                            snapshot.data.documents[index]
                                                ['imageUrl'][0],
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        );
                    }
                  }),
            ),
          ],
        ),
      ),
    );
  }
}

class ProductSearch extends SearchDelegate {
  //actions for app bar
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {},
      ),
    ];
  }

  //Leading icon on the left of the app bar
  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {},
    );
  }

  //show results based on the selection
  @override
  Widget buildResults(BuildContext context) {
    return null;
  }

  //Show when user give input
  @override
  Widget buildSuggestions(BuildContext context) {
    // TODO: implement buildSuggestions
    return null;
  }
}
