import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './homePage.dart';
import 'package:fluttertoast/fluttertoast.dart';

class GoogleSignUp extends StatefulWidget {
  final FirebaseUser user;

  const GoogleSignUp({Key key, this.user}) : super(key: key);

  @override
  _GoogleSignUpPageState createState() => _GoogleSignUpPageState();
}

class _GoogleSignUpPageState extends State<GoogleSignUp> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  ScrollController _listViewScrollController = new ScrollController();
  TextEditingController _usernameTextController = TextEditingController();
  TextEditingController _phoneNoTextController = TextEditingController();

  bool isLoading = false;
  String _username;
  String _phoneNumber;

  Future<void> signup() async {
    final formState = _formKey.currentState;
    setState(() {
      isLoading = true;
    });

    if (formState.validate()) {
      formState.save();
      try {
        if (widget.user != null) {
          //Insert the user to collection
          Firestore.instance
              .collection("users")
              .document(widget.user.uid)
              .setData({
            "id": widget.user.uid,
            "username": _username,
            "email": widget.user.email,
            "phone_number": _phoneNumber,
            "profilePicture": widget.user.photoUrl,
            "timestamp": Timestamp.now()
          });
        }

        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => HomePage(
                      user: widget.user,
                    )));
        Fluttertoast.showToast(
          msg: "Google gmail sign up successful",
        );
        setState(() {
          isLoading = false;
        });
      } catch (e) {
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(
          msg: "Account already exist",
        );
        print(e.message);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Fill in your information'),
        backgroundColor: Colors.pink[400],
      ),
      body: Container(
        color: Colors.white,
        child: Stack(
          children: <Widget>[
            Container(
              child: Center(
                child: Form(
                  key: _formKey,
                  child: ListView(
                    controller: _listViewScrollController,
                    shrinkWrap: false,
                    padding: EdgeInsets.only(left: 24.0, right: 24.0),
                    children: <Widget>[
                      //----------------Logo of the app----------------
                      SizedBox(height: 50.0),

                      CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 48.0,
                        child: Image.asset('assets/logo.PNG'),
                      ),

                      SizedBox(height: 50.0),

                      Center(
                        child: RichText(
                          text: TextSpan(children: <TextSpan>[
                            TextSpan(
                                text: 'Your registered gmail is: ',
                                style: TextStyle(
                                    fontSize: 15.0, color: Colors.black)),
                            TextSpan(
                                text: widget.user.email,
                                style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blue,
                                    decoration: TextDecoration.underline))
                          ]),
                        ),
                      ),

                      SizedBox(height: 30.0),
                      //----------------Username TextField--------------
                      TextFormField(
                        controller: _usernameTextController,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Can\'t be empty';
                          }else if(value.length < 6) {
                          return "Must be at least 6 characters long";
                      }
                        },
                        onSaved: (input) => _username = input,
                        style: TextStyle(color: Colors.black),
                        //initialValue: '********',
                        decoration: InputDecoration(
                          hintText: 'Username',
                          icon: Icon(
                            Icons.person_outline,
                            color: Colors.black,
                          ),
                          // contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                          // border: OutlineInputBorder(
                          //   borderRadius: BorderRadius.circular(32.0),
                          // )
                        ),
                      ),

                      SizedBox(height: 8.0),

                      TextFormField(
                        controller: _phoneNoTextController,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Phone number can\'t be empty ';
                          } else if (!RegExp(r"[0-9]").hasMatch(value)) {
                            return "Numbers only";
                          } else if (value.length < 10) {
                            return "Must be at least 10 numbers";
                          }
                        },
                        onSaved: (input) => _phoneNumber = input,
                        keyboardType: TextInputType.phone,
                        style: TextStyle(color: Colors.black),
                        //initialValue: '********',
                        decoration: InputDecoration(
                          hintText: 'Phone Number',
                          icon: Icon(
                            Icons.phone,
                            color: Colors.black,
                          ),
                          // contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                          // border: OutlineInputBorder(
                          //   borderRadius: BorderRadius.circular(32.0),
                          // )
                        ),
                      ),
                      SizedBox(height: 30.0),

                      //----------------Sign Up----------------
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 16.0),
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          shadowColor: Colors.lightBlueAccent.shade100,
                          child: MaterialButton(
                            minWidth: 200.0,
                            height: 42.0,
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                print('Validate Success');
                                signup();
                              }
                            },
                            color: Colors.pink[400],
                            child: Text('Sign Up',
                                style: TextStyle(color: Colors.white)),
                          ),
                        ),
                      ),

                      SizedBox(height: 8.0)
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              child: Visibility(
                visible: isLoading ?? true,
                child: Center(
                  child: SimpleDialog(
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      children: <Widget>[
                        Center(
                          //color: Colors.white.withOpacity(0.9),
                          child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.pink[400]),
                          ),
                        ),
                      ]),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
