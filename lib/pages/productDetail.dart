import 'package:flutter/material.dart';
import 'package:prelovedshop/db/product.dart';
import 'package:prelovedshop/pages/userProfile.dart';
//import 'package:prelovedshop/widgets/product.dart';
import './homePage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../db/users.dart';
import '../db/wishlist.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'sellerProfile.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:url_launcher/url_launcher.dart';
import 'dart:async';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';
import './searchPage.dart';
import './wishlist.dart';

class ProductDetail extends StatefulWidget {
  final productName;
  final productPrice;
  final productImage;
  final FirebaseUser user;
  final String username;
  final productId;

  const ProductDetail(
      {Key key,
      this.productName,
      this.productPrice,
      this.productImage,
      this.user,
      this.productId,
      this.username})
      : super(key: key);

  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  String _username = "";
  UserServices _userServices = UserServices();
  Firestore _firestore = Firestore.instance;
  ProductServices _productServices = ProductServices();
  WishListServices _wishlistServices = WishListServices();
  List<DocumentSnapshot> product = <DocumentSnapshot>[];
  //String _time = "";
  List<DocumentSnapshot> productinfo = <DocumentSnapshot>[];
  List<DocumentSnapshot> userinfo = <DocumentSnapshot>[];
  String _productUserId = "";
  String _productphoneNo = "";
  String _productId = "";
  String _productName = "";
  double _productPrice;
  String _productImage;
  bool productIsExist;
  QuerySnapshot wishlist;
  String _existingUserId;
  bool isLoading = false;

  Future launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceWebView: false, forceSafariVC: false);
    } else {
      print('Cant launcher url');
    }
  }

  @override
  void initState() {
    super.initState();
    isLoading = true;
    _getUserDetail();
    _getProductinfo();
    //_getProductDetail();
  }

  // _getProductDetail() async {
  //   QuerySnapshot data2 =
  //       await _productServices.getProductDetails(widget.productId);
  //   setState(() {
  //     product = data2.documents;
  //     print(product);
  //     _timestamp = product[0].data['timestamp'];
  //     print(_timestamp);
  //   });
  // }

  _getProductinfo() async {
    productinfo = await Firestore.instance
        .collection('products')
        .where('id', isEqualTo: widget.productId)
        .getDocuments()
        .then((snaps) {
      return snaps.documents;
    });

    setState(() {
      _productUserId = productinfo[0].data['userId'];
      _productId = productinfo[0].data['id'];
      _productName = productinfo[0].data['title'];
      _productPrice = productinfo[0].data['price'];
      _productImage = productinfo[0].data['imageUrl'][0];
      _getProductUserDetail();
      //print("Product name is: " + _productName);
    });
  }

  _getProductUserDetail() async {
    userinfo = await Firestore.instance
        .collection('users')
        .where('id', isEqualTo: _productUserId)
        .getDocuments()
        .then((snaps) {
      return snaps.documents;
    });
    if (userinfo != null) {
      setState(() {
        _productphoneNo = userinfo[0].data['phone_number'];
        //print("Product phone number is: " + _productphoneNo);
        isLoading = false;
      });
    }
    print("Userinfo is null");
    isLoading = false;
  }

  void _getUserDetail() async {
    List<DocumentSnapshot> userdata = await _userServices.getUser(widget.user);
    setState(() {
      _username = userdata[0].data['username'].toString();
      print("Log in username is: " + _username);
    });
  }

  _getDate(Timestamp t) {
    return timeago.format(t.toDate(), locale: 'en_short');
  }

  void _addProductToWishlist() async {
    _wishlistServices.addProductToWishlist(widget.user, _productUserId,
        _productId, _productName, _productPrice, _productImage);
    Fluttertoast.showToast(msg: "Item has been add to wishlist");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //elevation: 0.0,
        backgroundColor: Colors.pink[400],
        title: InkWell(
          child: Text('Product Detail'),
          onTap: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => HomePage()));
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.white),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SearchPage(user: widget.user)));
            },
          ),
          IconButton(
            icon: Icon(Icons.favorite_border, color: Colors.white),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => WishList(user: widget.user)));
            },
          ),
          // IconButton(
          //   icon: Icon(Icons.favorite_border, color: Colors.white),
          //   onPressed: () {},
          // ),
        ],
      ),
      bottomNavigationBar: Container(
        height: 35.0,
        color: Colors.white,
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: FlatButton.icon(
                onPressed: () async {
                  wishlist = await _firestore
                      .collection('wishlist')
                      .where("productId", isEqualTo: _productId)
                      .where("userId", isEqualTo: widget.user.uid)
                      .getDocuments();

                  // wishlistinfo = await Firestore.instance
                  //     .collection('wishlist')
                  //     .where('productName', isEqualTo: _productName)
                  //     .getDocuments();

                  // if (wishlistinfo.documents.length != 0) {
                  //   _wishlistProductName =
                  //       wishlistinfo.documents[0].data['productName'];
                  // }
                  //   print(_wishlistProductName);

                  if (wishlist.documents.length != 0) {
                    _existingUserId = wishlist.documents[0].data['userId'];
                  }
                  //print(_existingUserId);

                  if (_existingUserId == widget.user.uid) {
                    Fluttertoast.showToast(
                        msg: "Item had been already in the wishlist");
                    _existingUserId = "";
                  } else {
                    _addProductToWishlist();
                    _existingUserId = "";
                  }
                },
                label: Text(
                  "Add item to wish list",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.pink[400],
                icon: Icon(Icons.favorite_border, color: Colors.white),
              ),
            ),
            // Expanded(
            //   flex: 2,
            //   child: MaterialButton(
            //     onPressed: (){},
            //     child: Text(
            //       "Checkout",
            //       style: TextStyle(
            //         color: Colors.white
            //       ),
            //     ),
            //     color: Colors.pink[400],
            //   ),
            // ),
          ],
        ),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance
            .collection('products')
            .where('id', isEqualTo: widget.productId)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          }
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(
                child: SimpleDialog(
                    elevation: 0.0,
                    backgroundColor: Colors.transparent,
                    children: <Widget>[
                      Center(
                        //color: Colors.white.withOpacity(0.9),
                        child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.pink[400]),
                        ),
                      ),
                    ]),
              );
            default:
              return Stack(
                children: <Widget>[
                  ListView(
                    children: <Widget>[
                      Container(
                        height: 300.0,
                        child: GridTile(
                          footer: Container(
                            color: Colors.white70,
                            // child: ListTile(
                            //   leading: Text(
                            //     widget.productName,
                            //     style: TextStyle(
                            //       fontWeight: FontWeight.bold,
                            //       fontSize: 16.0,
                            //     ),
                            //   ),
                            //   title: Row(
                            //     children: <Widget>[
                            //       Expanded(
                            //         child: Column(
                            //           crossAxisAlignment: CrossAxisAlignment.end,
                            //           children: <Widget>[
                            //           Text(
                            //             "RM " + widget.productPrice.toString(),
                            //             style: TextStyle(
                            //               color: Colors.red,
                            //               fontWeight: FontWeight.bold
                            //             ),
                            //           )
                            //         ]),
                            //       )
                            //     ],
                            //   ),
                            // ),
                          ),
                          child: Carousel(
                            boxFit: BoxFit.fitHeight,
                            images: [
                              NetworkImage(
                                  snapshot.data.documents[0]['imageUrl'][0]),
                              NetworkImage(
                                  snapshot.data.documents[0]['imageUrl'][1]),
                              NetworkImage(
                                  snapshot.data.documents[0]['imageUrl'][2]),
                            ],
                            dotSize: 2.0,
                            //animationDuration: Duration(seconds: 5),
                            //animationCurve: Curves.fastOutSlowIn,
                            indicatorBgPadding: 6.0,
                            dotColor: Colors.pink[400],
                            dotBgColor: Colors.transparent,
                            autoplay: false,
                          ),

                          // Image.network(
                          //   snapshot.data.documents[0]['imageUrl'][0],
                          //   fit: BoxFit.cover,
                          // ),
                        ),
                      ),

                      Row(
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 7.0, horizontal: 15.0),
                              child: Text(
                                snapshot.data.documents[0]['title'],
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20.0,
                                ),
                              ))
                        ],
                      ),

                      Row(
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 0.0, horizontal: 15.0),
                              child: snapshot.data.documents[0]['price'] != 0
                                  ? Text(
                                      "RM " +
                                          snapshot.data.documents[0]['price']
                                              .toString() +
                                          "0",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20.0,
                                          color: Colors.red),
                                    )
                                  : Text(
                                      "Donation Item",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20.0,
                                          color: Colors.red),
                                    ))
                        ],
                      ),

                      Padding(
                        padding: EdgeInsets.only(right: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.only(left: 10.0),
                                    child:
                                        Icon(Icons.room, color: Colors.grey)),
                                Column(children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 15.0, horizontal: 0.0),
                                      child: Text(
                                        snapshot.data.documents[0]['location'],
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 13.0,
                                            color: Colors.grey),
                                      )),
                                ]),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.only(right: 2.0),
                                    child: Icon(Icons.schedule,
                                        color: Colors.grey)),
                                Column(children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 15.0, horizontal: 0.0),
                                      child: Text(
                                        _getDate(snapshot.data.documents[0]
                                                ['timestamp'])
                                            .toString(),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 13.0,
                                            color: Colors.grey),
                                      )),
                                ]),
                              ],
                            ),
                          ],
                        ),
                      ),

                      Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(left: 10, right: 0.0),
                            child: CircleAvatar(
                              backgroundImage: AssetImage('assets/avatar.png'),
                              //backgroundColor: Colors.grey,
                              //child: Icon(Icons.person, color: Colors.white),
                            ),
                          ),
                          FlatButton(
                            textColor: Colors.blue,
                            onPressed: () {
                              if (widget.user.uid !=
                                  snapshot.data.documents[0]['userId']) {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SellerProfile(
                                              userId: snapshot.data.documents[0]
                                                  ['userId'],
                                              user: widget.user,
                                            )));
                              } else {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => UserProfile(
                                              user: widget.user,
                                            )));
                              }
                            },
                            child: widget.user.uid ==
                                    snapshot.data.documents[0]['userId']
                                ? Text(_username,
                                    style: TextStyle(
                                        decoration: TextDecoration.underline))
                                : Text(snapshot.data.documents[0]['username'],
                                    style: TextStyle(
                                        decoration: TextDecoration.underline)),
                          )
                        ],
                      ),

                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 3,
                            child: Padding(
                                padding: EdgeInsets.only(
                                    top: 15.0, left: 15.0, bottom: 5.0),
                                child: Text(
                                  "Category",
                                  style: TextStyle(
                                      //fontWeight: FontWeight.bold,
                                      fontSize: 15.0,
                                      color: Colors.black),
                                )),
                          ),
                          Expanded(
                            flex: 3,
                            child: Padding(
                                padding:
                                    EdgeInsets.only(top: 15.0, bottom: 0.0),
                                child: Text(
                                  snapshot.data.documents[0]['category'],
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15.0,
                                      color: Colors.black),
                                )),
                          ),
                        ],
                      ),

                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 3,
                            child: Padding(
                                padding:
                                    EdgeInsets.only(left: 15.0, bottom: 10.0),
                                child: Text(
                                  "Condition",
                                  style: TextStyle(
                                      //fontWeight: FontWeight.bold,
                                      fontSize: 15.0,
                                      color: Colors.black),
                                )),
                          ),
                          Expanded(
                            flex: 3,
                            child: Padding(
                                padding: EdgeInsets.only(),
                                child: Text(
                                  snapshot.data.documents[0]['condition'],
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15.0,
                                      color: Colors.black),
                                )),
                          ),
                        ],
                      ),

                      Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(left: 15.0, bottom: 10.0),
                            child: Text(
                              "Contact: ",
                              style: TextStyle(
                                  //fontWeight: FontWeight.bold,
                                  fontSize: 15.0,
                                  color: Colors.black),
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.only(left: 115.0),
                              child: IconButton(
                                icon: Icon(
                                  FontAwesomeIcons.whatsappSquare,
                                  color: Colors.green.shade600,
                                ),
                                onPressed: () {
                                  launchURL("https://wa.me/6$_productphoneNo");
                                },
                                iconSize: 30.0,
                                alignment: Alignment.center,
                              )),
                          Padding(
                              padding: EdgeInsets.only(left: 0.0),
                              child: IconButton(
                                icon: Icon(
                                  FontAwesomeIcons.phoneSquare,
                                  color: Colors.blue.shade900,
                                ),
                                onPressed: () {
                                  launchURL("tel:6$_productphoneNo");
                                },
                                iconSize: 30.0,
                                alignment: Alignment.center,
                              )),
                        ],
                      ),

                      Divider(color: Colors.grey),

                      Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(left: 15.0, top: 5.0),
                            child: Text("Product Description",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17.0)),
                          )
                        ],
                      ),

                      SizedBox(height: 20.0),

                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                                padding: EdgeInsets.only(
                                    left: 15.0,
                                    top: 10.0,
                                    right: 15.0,
                                    bottom: 15.0),
                                child: Container(
                                  //height: 350.0,
                                  child: Text(snapshot.data.documents[0]
                                      ['description']),
                                )),
                          )
                        ],
                      ),

                      Divider(color: Colors.grey),

                      Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(
                                left: 15.0, top: 5.0, bottom: 25.0),
                            child: Text("Similar Products",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17.0)),
                          )
                        ],
                      ),

                      Container(
                        height: 360.0,
                        child: StreamBuilder<QuerySnapshot>(
                            stream: Firestore.instance
                                .collection('products')
                                .where('category',
                                    isEqualTo: snapshot.data.documents[0]
                                        ['category'])
                                .limit(10)
                                .snapshots(),
                            builder: (BuildContext context,
                                AsyncSnapshot<QuerySnapshot> snapshot) {
                              if (snapshot.hasError) {
                                return Text('Error: ${snapshot.error}');
                              }
                              switch (snapshot.connectionState) {
                                case ConnectionState.waiting:
                                  return Center(
                                    child: SimpleDialog(
                                        elevation: 0.0,
                                        backgroundColor: Colors.transparent,
                                        children: <Widget>[
                                          Center(
                                            //color: Colors.white.withOpacity(0.9),
                                            child: CircularProgressIndicator(
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                      Colors.pink[400]),
                                            ),
                                          ),
                                        ]),
                                  );
                                default:
                                  return Container(
                                    child: GridView.builder(
                                      itemCount: snapshot.data.documents.length,
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 2),
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return Card(
                                          child: Hero(
                                            tag: Text("Hero"),
                                            child: Material(
                                              child: InkWell(
                                                onTap: () => {
                                                  Navigator.of(context).push(
                                                      MaterialPageRoute(
                                                          //Passing values to the product detail page
                                                          builder: (context) =>
                                                              ProductDetail(
                                                                productId: snapshot
                                                                        .data
                                                                        .documents[
                                                                    index]['id'],
                                                                user:
                                                                    widget.user,
                                                              )))
                                                },
                                                child: GridTile(
                                                  footer: Container(
                                                    color: Colors.white70,
                                                    child: ListTile(
                                                      //Price condition
                                                      title:
                                                          snapshot.data.documents[
                                                                          index]
                                                                      [
                                                                      'price'] !=
                                                                  0
                                                              ? Text(
                                                                  "RM" +
                                                                      snapshot
                                                                          .data
                                                                          .documents[index]
                                                                              [
                                                                              'price']
                                                                          .toString() +
                                                                      "",
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                            .pink[
                                                                        400],
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500,
                                                                    fontSize:
                                                                        14.0,
                                                                  ),
                                                                )
                                                              : Text(
                                                                  "Donation",
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                            .pink[
                                                                        400],
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500,
                                                                    fontSize:
                                                                        12.5,
                                                                  ),
                                                                ),
                                                      // ===========================
                                                      leading: Container(
                                                          width: 75.0,
                                                          child: Padding(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      vertical:
                                                                          4.0),
                                                              child: Text(
                                                                  snapshot.data
                                                                              .documents[
                                                                          index]
                                                                      ['title'],
                                                                  style:
                                                                      TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        14.0,
                                                                  )))),
                                                    ),
                                                  ),
                                                  child: Image.network(
                                                    snapshot.data
                                                            .documents[index]
                                                        ['imageUrl'][0],
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  );
                              }
                            }),
                      ),

// ----------------- Variant BUTTON ------------------------
                      // Padding(
                      //   padding: EdgeInsets.only(top: 20.0,),
                      //   child: Row(
                      //     children: <Widget>[
                      //       Expanded(
                      //         child: MaterialButton(
                      //           onPressed: () {
                      //             showDialog(
                      //                 context: context,
                      //                 builder: (context) => AlertDialog(
                      //                       title: Text("Size"),
                      //                       content: Text("Choose the size"),
                      //                       actions: <Widget>[
                      //                         MaterialButton(
                      //                           child: Text("close"),
                      //                           onPressed: () {
                      //                             Navigator.of(context).pop(context);
                      //                           },
                      //                         )
                      //                       ],
                      //                     ));
                      //           },
                      //           color: Colors.white,
                      //           textColor: Colors.grey,
                      //           elevation: 0.2,
                      //           child: Row(
                      //             children: <Widget>[
                      //               Expanded(
                      //                 child: Text("Size"),
                      //               ),
                      //               Expanded(
                      //                 child: Icon(Icons.arrow_drop_down),
                      //               ),
                      //             ],
                      //           ),
                      //         ),
                      //       ),
                      //       Expanded(
                      //         child: MaterialButton(
                      //           onPressed: () {
                      //             showDialog(
                      //                 context: context,
                      //                 builder: (context) => AlertDialog(
                      //                       title: Text("Color"),
                      //                       content: Text("Choose the color"),
                      //                       actions: <Widget>[
                      //                         MaterialButton(
                      //                           onPressed: () {
                      //                             Navigator.of(context).pop(context);
                      //                           },
                      //                           child: Text("close"),
                      //                         )
                      //                       ],
                      //                     ));
                      //           },
                      //           color: Colors.white,
                      //           textColor: Colors.grey,
                      //           elevation: 0.2,
                      //           child: Row(
                      //             children: <Widget>[
                      //               Expanded(
                      //                 child: Text("Color"),
                      //               ),
                      //               Expanded(
                      //                 child: Icon(Icons.arrow_drop_down),
                      //               ),
                      //             ],
                      //           ),
                      //         ),
                      //       ),
                      //       Expanded(
                      //         child: MaterialButton(
                      //           onPressed: () {
                      //             showDialog(
                      //                 context: context,
                      //                 builder: (context) => AlertDialog(
                      //                       title: Text("Quantity"),
                      //                       content: Text("Choose the quantity"),
                      //                       actions: <Widget>[
                      //                         MaterialButton(
                      //                           onPressed: () {
                      //                             Navigator.of(context).pop(context);
                      //                           },
                      //                           child: Text("close"),
                      //                         )
                      //                       ],
                      //                     ));
                      //           },
                      //           color: Colors.white,
                      //           textColor: Colors.grey,
                      //           elevation: 0.2,
                      //           child: Row(
                      //             children: <Widget>[
                      //               Expanded(
                      //                 child: Text("Qty"),
                      //               ),
                      //               Expanded(
                      //                 child: Icon(Icons.arrow_drop_down),
                      //               ),
                      //             ],
                      //           ),
                      //         ),
                      //       ),
                      //     ],
                      //   ),
                      // ),
                    ],
                  ),
                  Positioned(
                    child: Container(
                      color: Colors.grey[100],
                      child: Visibility(
                        visible: isLoading ?? true,
                        child: Center(
                          child: SimpleDialog(
                              elevation: 0.0,
                              backgroundColor: Colors.transparent,
                              children: <Widget>[
                                Center(
                                  //color: Colors.white.withOpacity(0.9),
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.pink[400]),
                                  ),
                                ),
                              ]),
                        ),
                      ),
                    ),
                  )
                ],
              );
          }
        },
      ),
    );
  }
}

class SimilarProduct extends StatefulWidget {
  @override
  _SimilarProductState createState() => _SimilarProductState();
}

class _SimilarProductState extends State<SimilarProduct> {
  var product_list = [
    {
      "name": "Polo",
      "picture": "assets/products/clothes1.jpeg",
      "price": 99,
    },
    {
      "name": "Adidas Ultra Boost",
      "picture": "assets/products/shoes2.jpg",
      "price": 0,
    },
    {
      "name": "Adidas Backpack",
      "picture": "assets/products/bag1.jpg",
      "price": 130,
    },
    {
      "name": "Adidas Backpack 2",
      "picture": "assets/products/bag2.jpg",
      "price": 0,
    },
    {
      "name": "Adidas Backpack 2",
      "picture": "assets/products/bag2.jpg",
      "price": 0,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: product_list.length,
      gridDelegate:
          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemBuilder: (BuildContext context, int index) {
        return SimilarSingleProduct(
          productName: product_list[index]['name'],
          productPicture: product_list[index]['picture'],
          productPrice: product_list[index]['price'],
        );
      },
    );
  }
}

class SimilarSingleProduct extends StatelessWidget {
  final productName;
  final productPicture;
  final productPrice;

  const SimilarSingleProduct(
      {Key key, this.productName, this.productPicture, this.productPrice})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Hero(
        tag: Text("Hero"),
        child: Material(
          child: InkWell(
            onTap: () => {
              Navigator.of(context).push(MaterialPageRoute(
                  //Passing values to the product detail page
                  builder: (context) => ProductDetail(
                        productName: this.productName,
                        productPrice: this.productPrice,
                        productImage: this.productPicture,
                      )))
            },
            child: GridTile(
              footer: Container(
                  color: Colors.white70,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          productName,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                      //ProductPrice Condition
                      productPrice != 0
                          ? Text(
                              "RM " + productPrice.toString(),
                              style: TextStyle(
                                  color: Colors.pink[400],
                                  fontWeight: FontWeight.w500,
                                  fontSize: 15.0),
                            )
                          : Text(
                              "Donation",
                              style: TextStyle(
                                  color: Colors.pink[400],
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.0),
                            ),
                    ],
                  )

                  // ListTile(
                  //   //Price condition
                  //   title: productPrice != 0 ?
                  //   Text("RM "+productPrice.toString(), style: TextStyle(
                  //     color: Colors.pink[400],
                  //     fontWeight: FontWeight.w500,
                  //     fontSize: 15.0,
                  //     ),
                  //   )
                  //   : Text("Donation", style: TextStyle(
                  //       color: Colors.pink[400],
                  //       fontWeight: FontWeight.w500,
                  //       fontSize: 14.0,
                  //     ),
                  //   ),
                  //   // ===========================
                  //   leading: Container(
                  //     width: 80.0,
                  //     child: Padding(
                  //       padding: EdgeInsets.symmetric(vertical: 4.0),
                  //       child: Text(productName, style: TextStyle(
                  //         fontWeight: FontWeight.bold,
                  //         fontSize: 14.0,
                  //         )
                  //       )
                  //     )
                  //   ),
                  // ),
                  ),
              child: Image.asset(
                productPicture,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
