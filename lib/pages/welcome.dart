import 'package:flutter/material.dart';
import 'package:prelovedshop/pages/signUpPage.dart';
//import './loginPage.dart';
import './signUpPage.dart';
import './homePage.dart';

class WelcomePage extends StatefulWidget {
  final String title;
  WelcomePage(this.title);
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          RaisedButton(
              onPressed: navigateToLogin,
              child: Text("Log In"),
          ),
          RaisedButton(
              onPressed: navigateToSignUp,
              child: Text("Sign Up"),
          )
        ],
      ),
    );
  }

  void navigateToLogin(){
    Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
  }

  void navigateToSignUp(){
    Navigator.push(context, MaterialPageRoute(builder: (context)=>SignUpPage()));
  }
}